function sendAjax(d, b, e, a, i, f) {
    if (d.hasClass("disabled")) {
        return false
    }
    if (typeof f === "undefined") {
        f = {}
    }
    var c = jQuery(d);
    var g = c.offset();
    var j = (typeof f.spinnerVisible !== "undefined" ? f.spinnerVisible : true);
    var k = (typeof f.enableAgain !== "undefined" ? f.enableAgain : true);
    if (j) {
        var h = jQuery('<div class="spinner-img">').css("position", "absolute").hide().appendTo("body")
    }
    e = e + "&a=" + new Date().getTime();
    e = e + "&sh=" + secureHash;
    jQuery.ajax({
        url: b,
        data: e,
        dataType: (typeof f.dataType !== "undefined" ? f.dataType : ""),
        type: (typeof f.type !== "undefined" ? f.type : "POST"),
        beforeSend: function() {
            if (j) {
                h.css({
                    "z-index": 999,
                    top: g.top + (c.height() - h.height()) / 2,
                    left: g.left + (c.width() - h.width()) / 2
                }).fadeIn()
            }
            d.addClass("disabled")
        }
    }).done(function(l) {
        if (a !== undefined && typeof a === "function") {
            a(l, d)
        }
    }).fail(function(l, n, m) {
        if (i !== undefined && typeof i === "function") {
            i(d, l, n, m)
        }
    }).always(function() {
        if (j) {
            h.fadeOut(function() {
                h.remove()
            })
        }
        if (k) {
            d.removeClass("disabled")
        }
    })
}
function ajaxSaveMsg(e, a, d) {
    jQuery("#spinner").finish().fadeOut("fast");
    var b = e.find(".ajaxSaveMsg").attr("class", "ajaxSaveMsg " + a)
      , c = b.finish().html(d).width();
    b.css({
        width: 0,
        opacity: 1
    }).animate({
        width: c
    }).delay(2000).animate({
        opacity: 0
    })
}
function doneCallback(b, a) {
    ajaxSaveMsg(a, "done", lang.SAVED)
}
function failCallback(a) {
    ajaxSaveMsg(a, "fail", lang.CANNOT_SAVE)
}
function sendRequest(method, url, data, showSpinnerId, spinnerClass, spinnerImage, spinnerContent) {
    var loadingSpinner = null
      , request = {
        method: method,
        url: url,
        data: data + "&a=" + new Date().getTime() + "&sh=" + secureHash,
        onComplete: function(response) {
            if (null !== loadingSpinner) {
                loadingSpinner.hide()
            }
            eval(response)
        }
    };
    if (showSpinnerId) {
        var showSpinnerObject = $(showSpinnerId);
        if (showSpinnerObject) {
            var params = {
                img: {},
                content: {}
            };
            if (spinnerImage) {
                params.img["class"] = spinnerImage
            }
            if (spinnerContent) {
                params.content["class"] = spinnerContent
            }
            if (spinnerClass) {
                params["class"] = spinnerClass
            }
            loadingSpinner = new Spinner(showSpinnerId,params);
            request.onRequest = loadingSpinner.show()
        }
    }
    new Request(request).send()
}
function updatePlayerStatus(c) {
    var b = jQuery;
    for (var a in c) {
        if (!c.hasOwnProperty(a)) {
            continue
        }
        var d = c[a].value;
        if (c[a].maxValue) {
            d += " - " + c[a].maxValue
        }
        if ("leben" === a) {
            d += " %"
        }
        b("#char_" + a).text(d);
        tooltips.set(b("#char_" + a + "_tt"), c[a].tooltip);
        b("#char_" + a + "_balken").css("width", c[a].value + "%")
    }
}
function updatePlayerSkills(c) {
    var b = jQuery;
    for (var a in c) {
        if (!c.hasOwnProperty(a)) {
            continue
        }
        b("#char_f" + a).text(c[a].name);
        tooltips.set(b("#char_f" + a + "_tt"), c[a].tooltip);
        b("#charbalken_f" + a).css("width", c[a].value + "%")
    }
}
;;function mmoDetectDeviceOS() {
    return (function(a) {
        if (/iPhone/i.test(a) || /iPad/.test(a) || /iPod/.test(a)) {
            return "ios"
        } else {
            if (/Android/.test(a)) {
                return "android"
            } else {
                if (/Windows Phone OS 7\.0/.test(a)) {
                    return "winphone7"
                } else {
                    if (/BlackBerry/.test(a)) {
                        return "rim"
                    } else {
                        return "desktop"
                    }
                }
            }
        }
    }
    )(navigator.userAgent)
}
function mmoSetBannerDeviceOS(d, c, a, b) {
    d = d.replace("{$random_number}", new Date().getTime());
    d = d.replace("{$os}", c);
    d = d.replace("{$kid}", a);
    document.getElementById(b).innerHTML = d
}
;;var blackoutDialogDivId;
var confirmed;
var blackoutScreen = false;
function blackoutDialog(switchOn, dialogDivId, msieAbsPosY, abortFunction, canCancel) {
    if (dialogDivId != undefined) {
        var dialogDivObj = document.getElementById(dialogDivId);
        if (!dialogDivObj.parentNode.tagName.match(/^body$/i)) {
            dialogDivObj.parentNode.removeChild(dialogDivObj);
            document.body.appendChild(dialogDivObj)
        }
    }
    if (switchOn == true && document.getElementById("breakDiv")) {
        blackoutDialog(false, blackoutDialogDivId, msieAbsPosY)
    }
    if (switchOn == true) {
        blackoutDialogDivId = dialogDivId;
        var div = document.createElement("div");
        div.id = "breakDiv";
        if (canCancel != undefined && canCancel == false) {
            div.className = "break_div break_div2"
        } else {
            div.onmousedown = function() {
                blackoutDialog(false);
                if (abortFunction) {
                    eval(abortFunction)
                }
            }
            ;
            div.className = "break_div"
        }
        var width, height;
        if (navigator.userAgent.indexOf("MSIE") >= 0) {
            width = document.documentElement.clientWidth;
            height = document.documentElement.clientHeight
        } else {
            width = window.innerWidth;
            height = window.innerHeight
        }
        div.style.zIndex = 500;
        div.style.overflow = "hidden";
        if (navigator.userAgent.indexOf("MSIE 6.0") >= 0) {
            document.body.style.width = "100%";
            document.body.style.height = "100%";
            div.style.width = document.body.offsetWidth + "px";
            div.style.height = document.body.offsetHeight + "px"
        } else {
            div.style.position = "fixed";
            div.style.width = "100%";
            div.style.height = "100%"
        }
        document.body.appendChild(div);
        document.getElementById(dialogDivId).style.display = "block";
        var top;
        if ((navigator.userAgent.indexOf("MSIE") >= 0) && (navigator.userAgent.indexOf("MSIE 8") < 0)) {
            var windowHeight = document.documentElement.clientHeight;
            var popupHeight = document.getElementById(dialogDivId).offsetHeight;
            var scrolled = document.documentElement.scrollTop;
            top = Math.floor((windowHeight / 2) - (popupHeight / 2)) + scrolled
        } else {
            top = Math.floor(height / 2) - Math.floor(document.getElementById(dialogDivId).offsetHeight / 2)
        }
        var left = Math.floor((width - document.getElementById(dialogDivId).offsetWidth) / 2);
        if ($(document.body).getStyle("direction") == "rtl") {
            document.getElementById(dialogDivId).style.right = left + "px"
        } else {
            document.getElementById(dialogDivId).style.left = left + "px"
        }
        document.getElementById(dialogDivId).style.top = top + "px";
        document.getElementById(dialogDivId).style.zIndex = 501
    } else {
        if (document.getElementById("breakDiv")) {
            document.body.removeChild(document.getElementById("breakDiv"))
        }
        document.getElementById(blackoutDialogDivId).style.display = "none";
        document.getElementById(blackoutDialogDivId).style.zIndex = -1
    }
    blackoutScreen = switchOn
}
function BlackoutDialog(dialogDiv, canCancel, resultSelector) {
    var dialog = jQuery("#" + dialogDiv);
    if (!dialog.length) {
        dialog = jQuery("#blackoutDialog" + dialogDiv)
    }
    blackoutDialog(true, dialog.attr("id"), "ignored", null, canCancel);
    var header = dialog.find("#header_" + dialogDiv)
      , confirmBtn = dialog.find("#link" + dialogDiv)
      , cancelBtn = dialog.find("#linkcancel" + dialogDiv)
      , buttons = confirmBtn.add(cancelBtn)
      , contentDiv = dialog.find("#dialogTxt" + dialogDiv)
      , pictureDiv = dialog.find("#picture" + dialogDiv)
      , resultElement = null
      , setAttribute = function(el, attr) {
        return function(val) {
            el.attr(attr, val)
        }
    }
      , registerCallback = function(el) {
        return function(func) {
            el.off("click.bd");
            if (func) {
                if ("string" === typeof func) {
                    var code = func;
                    func = function() {
                        eval(code)
                    }
                }
                el.on("click.bd", function() {
                    func.call(dialog, resultElement.val())
                })
            }
        }
    }
      , setters = {
        title: header.text.bind(header),
        text: contentDiv.html.bind(contentDiv),
        picture: function(src) {
            pictureDiv.empty();
            if (src) {
                pictureDiv.append(jQuery("<img>").attr({
                    alt: "",
                    src: src
                }))
            }
        },
        confirmText: confirmBtn.val.bind(confirmBtn),
        cancelText: function(cancel) {
            if (cancel) {
                cancelBtn.val(cancel)
            }
            cancelBtn.parent().css("display", !!cancel ? "block" : "none")
        },
        confirmAction: registerCallback(confirmBtn),
        cancelAction: registerCallback(cancelBtn),
        defaultValue: function(value) {
            resultElement.val(value).focus().select()
        }
    };
    function init() {
        if (resultElement) {
            resultElement.off("keyup.bd")
        }
        resultElement = dialog.find(resultSelector || "input").not(buttons).first();
        resultElement.on("keyup.bd", function(e) {
            if (e.which == 13) {
                confirmBtn.click();
                return false
            } else {
                if (e.which == 27) {
                    cancelBtn.click();
                    return false
                }
            }
        });
        setters.minimumValue = setAttribute(resultElement, "min");
        setters.maximumValue = setAttribute(resultElement, "max");
        buttons.off("click.bdOff").on("click.bdOff", function() {
            blackoutDialog(false)
        })
    }
    init();
    if (undefined === canCancel || canCancel) {
        jQuery("#breakDiv").attr("onmousedown", "").click(function() {
            cancelBtn.click()
        })
    }
    this.set = function(parameter, value) {
        if (1 === arguments.length) {
            for (var key in parameter) {
                if (parameter.hasOwnProperty(key)) {
                    this.set(key, parameter[key])
                }
            }
        } else {
            if (setters[parameter]) {
                setters[parameter](value)
            }
        }
        return this
    }
    ;
    this.getNode = function() {
        return dialog
    }
    ;
    this.confirm = function(confirm) {
        return this.set("confirmAction", confirm)
    }
    ;
    this.cancel = function(cancel) {
        return this.set("cancelAction", cancel)
    }
    ;
    this.min = function(min) {
        return this.set("minimumValue", min)
    }
    ;
    this.max = function(min) {
        return this.set("maximumValue", min)
    }
    ;
    this.val = function(val) {
        return this.set("defaultValue", val)
    }
    ;
    this.fromDialog = function(elem) {
        this.set({
            title: elem.attr("title"),
            text: elem.html()
        });
        init();
        return this
    }
}
function blackoutDialogFlex(onClickAction, diaTxt, switchOn, dialogDivId, msieAbsPosY, abortFunction) {
    document.getElementById("link" + dialogDivId).onclick = function() {
        eval(onClickAction)
    }
    ;
    if (diaTxt != "") {
        document.getElementById("dialogTxt" + dialogDivId).innerHTML = diaTxt
    }
    blackoutDialog(switchOn, "blackoutDialog" + dialogDivId, msieAbsPosY, abortFunction)
}
;;(function(a) {
    window.ContinuousTimer = function(g, b, c) {
        b = (b || 1000);
        var e = (c || 0);
        var d = (new Date).getTime();
        function f() {
            var i = (new Date).getTime();
            var h = i - d;
            e += h;
            g(h, e);
            d = i;
            setTimeout(f, Math.max(0, b - e % b))
        }
        a(window).on("pageshow", function(h) {
            if (h.originalEvent.persisted) {
                f()
            }
        });
        f()
    }
}
)(jQuery);
;jQuery(function() {
    var $ = jQuery;
    var shiftState = false
      , controlState = false
      , draggingElement = null;
    $(document).on("keydown keyup", function(ev) {
        var newShift = !!ev.shiftKey
          , newControl = !!ev.ctrlKey;
        var stateChanged = newControl != controlState;
        if (shiftState != newShift) {
            var check = $("#moveStack");
            check.prop("checked", !check.prop("checked"));
            stateChanged = true
        }
        shiftState = newShift;
        controlState = newControl;
        if (stateChanged) {
            $(".ui-draggable-dragging").trigger("refreshAmount")
        }
    });
    $(document).on("dragstart", function(ev) {
        draggingElement = $(ev.target)
    }).on("dragend", function() {
        draggingElement = null;
        $(".image-grayed").removeClass("image-grayed")
    });
    function Draggable() {
        this.start = function() {
            $(this).trigger("dragstart");
            if ("default" != $(this).draggable("option", "scope")) {
                return
            }
            Tool.rebuildGridDropareas();
            $(".ui-droppable.grid-droparea:not(.active)").addClass("image-grayed");
            $(".ui-droppable.ui-draggable-handle:not(.active)").addClass("image-grayed")
        }
        ;
        this.stop = function() {
            $(document).trigger("dragend");
            $(".ui-droppable.active").removeClass("active")
        }
        ;
        this.helper = function() {
            return (new Helper($(this))).getElement()
        }
        ;
        this.opacity = 0.75;
        this.revert = "invalid";
        this.zIndex = 999
    }
    function Droppable(isGrid, isGreedy) {
        function canBeStacked(drag, drop) {
            if (drop.data("noStack") || drop.parent().data("noStack")) {
                return false
            }
            if (drag.data("noDestack") || drag.parent().data("noDestack")) {
                if (drag.data("amount") + drop.data("amount") > 100) {
                    return false
                }
            }
            var hashaccept = drop.data("hash");
            return (hashaccept && (drag.data("hash") == hashaccept))
        }
        function canBeFused(drag, drop) {
            if (drop.data("noFuse") || drop.parent().data("noFuse")) {
                return false
            }
            if (drag.data("amount") < drop.data("amount")) {
                return false
            }
            var ct = drop.data("contentType") || 0;
            var ct2 = drag.data("contentType") || 0;
            if (64 == ct && 64 == ct2) {
                var levelOk = drop.data("level") <= drag.data("level")
                  , canUse = Tool.inArray(drag.data("basis"), drop.data("usableWithBasis"));
                if (levelOk && canUse) {
                    return true
                }
            }
            var ctenchant = drag.data("enchantType") || 0;
            return !!(ct & ctenchant)
        }
        function canBeCombined(drag, drop) {
            if (drop.data("noCombine") || drop.parent().data("noCombine")) {
                return false
            }
            if (-1 == drag.data("contentType")) {
                return true
            }
            return canBeStacked(drag, drop) || canBeFused(drag, drop)
        }
        function canBeMoved(drag, drop) {
            var ct = drag.data("contentType") || 0;
            var ctaccept = drop.data("contentTypeAccept") || 0;
            if (!(ct & ctaccept)) {
                return false
            }
            var isEq = Tool.getContainerType(drop) == Tool.CONTAINER_EQUIPMENT
              , sbTo = drag.data("soulboundTo");
            if (isEq && sbTo && playerId != sbTo) {
                return false
            }
            return !drop.is(".ui-draggable")
        }
        function checkPosition(ui) {
            var oldContainer = ui.draggable.parent()
              , newContainer = $(this).parent()
              , position = $(this).data("dropPositioning").getClosestValidPosition(newContainer, ui)
              , w = 32 * ui.draggable.data("measurementX")
              , h = 32 * ui.draggable.data("measurementY")
              , ol = ui.draggable.position().left
              , ot = ui.draggable.position().top
              , or = ol + w
              , ob = ot + h
              , nl = position.left
              , nt = position.top
              , nr = nl + w
              , nb = nt + h;
            return [position, (oldContainer[0] != newContainer[0] || nl >= or || nt >= ob || nr <= ol || nb <= ot)]
        }
        function basicAcceptHandler(drag) {
            var origin = drag.data("origin");
            if (origin && origin[0] === this) {
                return true
            }
            if (drag.data("isOrigin") || $(this).data("isOrigin")) {
                return false
            }
            return !(origin && origin.data("amount") > 0)
        }
        function acceptHandler(drag) {
            if (!basicAcceptHandler.call(this, drag)) {
                return false
            }
            var combine = canBeCombined(drag, $(this))
              , move = canBeMoved(drag, $(this))
              , ch = $(this).children()
              , isEmpty = (0 == ch.length)
              , acceptFunc = Tool.getFunctionReference($(this).data("acceptFunction"))
              , isAccepted = acceptFunc ? acceptFunc.call($(this), drag) : true;
            if (!isEmpty) {
                isEmpty = (0 == ch.data("amount") && drag.data("itemId") == ch.data("itemId"))
            }
            return (combine || (move && isEmpty)) && isAccepted
        }
        function dropHandler(event, ui) {
            var drop = $(this)
              , position = {
                left: 0,
                top: 0
            };
            if ("topleft" != drop.data("align")) {
                position.left = (drop.width() - 32 * ui.draggable.data("measurementX")) / 2;
                position.top = (drop.height() - 32 * ui.draggable.data("measurementY")) / 2
            }
            (new DropAction(ui.draggable,$(this),position)).execute()
        }
        function gridDropHandler(event, ui) {
            var position = checkPosition.call(this, ui);
            (new DropAction(ui.draggable,$(this).parent(),position[0])).execute(!position[1])
        }
        this.accept = isGrid ? basicAcceptHandler : acceptHandler;
        this.activeClass = "active";
        this.drop = isGrid ? gridDropHandler : dropHandler;
        this.greedy = isGreedy;
        this.tolerance = "pointer";
        this.over = function(e, ui) {
            var overlap = false;
            if (isGrid) {
                var check = checkPosition.call(this, ui);
                overlap = !check[1]
            }
            ui.helper.trigger("refreshAmount", [this, overlap])
        }
        ;
        this.out = function(e, ui) {
            ui.helper.trigger("removeAmount", this)
        }
    }
    function Helper(drag) {
        var element = $("<div>").addClass("item-i-" + drag.data("basis")).css("pointer-events", "none").appendTo("body");
        if (drag.data("amount") > 1) {
            var over = null
              , noDestacking = true;
            element.on("refreshAmount", function(ev, newOver, newNoDestack) {
                if (newOver) {
                    over = newOver;
                    noDestacking = !!newNoDestack
                }
                if (over) {
                    var dragProperties = new DragProperties(drag,$(over),noDestacking);
                    element.attr("data-amount", dragProperties.getAmount() || "#")
                } else {
                    element.removeAttr("data-amount")
                }
            });
            element.on("removeAmount", function(ev, currentOver) {
                if (over == currentOver) {
                    over = null;
                    element.removeAttr("data-amount")
                }
            });
            element.trigger("refreshAmount")
        }
        this.getElement = function() {
            return element
        }
    }
    function DropAction(drag, drop, positionTo) {
        var dragFrom = drag.parent()
          , positionFrom = drag.position()
          , type = "move";
        function getDropFunction(dropAction) {
            var dropFunction = drop.data("dropFunction");
            if (!dropFunction && drop.is(".grid-droparea")) {
                dropFunction = drop.parent().data("dropFunction")
            }
            var fun = Tool.getFunctionReference(dropFunction);
            if (fun) {
                dropFunction = fun
            }
            if ("string" == typeof dropFunction) {
                var url = dropFunction + "&iid=" + drag.data("itemId");
                dropFunction = function(drag, amount) {
                    dropAction.defaultDropFunction(drag, amount, url)
                }
            }
            return dropFunction
        }
        function doExecute(amountToMove) {
            var amountOnStack = drag.data("amount");
            amountToMove = Math.max(0, Math.min(amountOnStack, amountToMove));
            if (0 == amountToMove) {
                return
            }
            var drag2 = null;
            if (drop.is(".ui-draggable")) {
                drag2 = drop
            } else {
                var data = $.extend({}, drag.data());
                delete data.uiDraggable;
                delete data.uiDroppable;
                data.amount = 0;
                data.positionX = drop.data("positionX") || 1;
                data.positionY = drop.data("positionY") || 1;
                if (drop.is(".ui-droppable-grid")) {
                    data.positionX += positionTo.left / 32;
                    data.positionY += positionTo.top / 32
                }
                drag2 = drag.clone().data(data).css(positionTo).appendTo(drop);
                DragDrop.makeDraggable(drag2);
                DragDrop.makeDroppable(drag2)
            }
            var callback = getDropFunction(this) || this.defaultDropFunction;
            if (drop.data("keepOriginalPosition")) {
                drag.data("isOrigin", true);
                drag2.data("origin", drag)
            }
            if (callback.call(this, drag2, amountToMove)) {
                this.callPostDragFunctions(drag2, amountToMove)
            }
        }
        this.execute = function(noDestacking) {
            var dropAction = this;
            if (dragFrom[0] == drop[0] && positionFrom.left == positionTo.left && positionFrom.top == positionTo.top) {
                return
            }
            var dragProperties = new DragProperties(drag,drop,noDestacking);
            type = dragProperties.type;
            function callback(amount) {
                amount = amount || dragProperties.getAmount();
                if (null !== amount) {
                    doExecute.call(dropAction, amount);
                    return amount
                } else {
                    (new BlackoutDialog("destackConfirm")).confirm(callback).max(dragProperties.amountAllowed).val(dragProperties.amountAllowed);
                    return "async"
                }
            }
            if (dragProperties.getSoulbindRequired()) {
                (new BlackoutDialog("soulboundConfirm")).confirm(callback);
                return "async"
            }
            return callback()
        }
        ;
        this.getDrag = function() {
            return drag
        }
        ;
        this.getDragContainer = function() {
            return dragFrom
        }
        ;
        this.getDrop = function() {
            return drop
        }
        ;
        this.getDropContainer = function() {
            return drop.is(".ui-draggable") ? drop.parent() : drop
        }
        ;
        this.getDropType = function() {
            return type
        }
        ;
        this.getDropOffset = function() {
            var offset = drop.offset();
            offset.left += positionTo.left;
            offset.top += positionTo.top;
            offset.opacity = 1;
            return offset
        }
        ;
        this.getOriginalOffset = function() {
            return drag.data("ui-draggable").originalPosition
        }
    }
    DropAction.prototype.sendRequest = function(to, amount, requestUrl) {
        function toggleDragDrop(el, status) {
            el.removeClass("disabled");
            if (!status) {
                el.addClass("disabled")
            }
            var func = !!status ? "enable" : "disable";
            if (el.is(".ui-draggable")) {
                el.draggable(func)
            }
            if (el.is(".ui-droppable")) {
                el.droppable(func)
            }
        }
        function applyResponse(el, response) {
            if (response) {
                if (response.data) {
                    el.removeClass("item-i-" + el.data("basis"));
                    el.data(response.data);
                    el.addClass("item-i-" + el.data("basis"));
                    tooltips.init(el)
                }
                if (el.parent().data("noRemove")) {
                    el.removeClass("ui-draggable");
                    el.draggable("destroy")
                }
                toggleDragDrop(el, true);
                Tool.setAmount(from)
            } else {
                el.removeClass("disabled").remove()
            }
        }
        function done(data) {
            var target = temp.is(":visible") ? temp : to;
            if (data.error) {
                var plingX = target.offset().left - (95 - (target.width() / 2));
                pling.item(plingX, target.offset().top, data.error, "red")
            }
            if (data.heal) {
                pling.portrait(data.heal, "#00FF21", "2em")
            }
            if (data.gold || data.rubies) {
                plingX = target.offset().left - (75 - (target.width() / 2));
                pling.currency(data.gold, data.rubies, plingX, target.offset().top)
            }
            if (data.needsReload) {
                location.reload()
            }
            if (data.error) {
                fail();
                return
            }
            var originContainer = from.parent();
            headerObject.update(data);
            updatePlayerStatus(data.status || []);
            updatePlayerSkills(data.skills || []);
            if (data.hasOwnProperty("from")) {
                applyResponse(from, data.from)
            }
            if (data.hasOwnProperty("to")) {
                applyResponse(to, data.to)
            }
            temp.queue(function() {
                $(this).remove().dequeue()
            });
            da.callPostDragFunctions(to, amount);
            DragDrop.refreshDroppables(to.parent().add(originContainer))
        }
        function fail(el, data) {
            if (data && !data.getAllResponseHeaders()) {
                return
            }
            if (el && data) {
                if (200 == data.status && "" != data.responseText) {
                    try {
                        eval(data.responseText);
                        return
                    } catch (err) {}
                }
                location.reload()
            }
            Tool.setAmount(to, originalAmountTo);
            temp.stop().show();
            if (0 == originalAmountTo) {
                to.remove()
            } else {
                toggleDragDrop(to, true)
            }
            temp.animate(originalOffset, 500, function() {
                Tool.setAmount(from, originalAmountFrom);
                toggleDragDrop(from, true);
                temp.remove()
            })
        }
        if (!requestUrl) {
            requestUrl = "ajax.php?mod=inventory&submod=move";
            var shopType = to.parent().data("shopType");
            if (shopType) {
                requestUrl += "&shopType=" + shopType
            }
        }
        var da = this
          , temp = this.temp
          , from = this.getDrag()
          , originalOffset = this.getOriginalOffset()
          , originalAmountTo = to.data("amount")
          , originalAmountFrom = from.data("amount") + amount
          , cont = this.getDropContainer()
          , doll = $("#plDoll")
          , data = {
            from: from.data("containerNumber"),
            fromX: from.data("positionX"),
            fromY: from.data("positionY"),
            to: cont.data("containerNumber"),
            toX: to.data("positionX"),
            toY: to.data("positionY"),
            amount: amount
        };
        if (doll.length) {
            data.doll = doll.val()
        }
        toggleDragDrop(from, false);
        toggleDragDrop(to, false);
        sendAjax(temp, requestUrl + "&" + $.param(data), "", done, fail, {
            dataType: "json",
            spinnerVisible: false
        })
    }
    ;
    DropAction.prototype.defaultDropFunction = function(to, amount, requestUrl) {
        function requestNeeded() {
            var realDrag = this.getDrag();
            var origin = realDrag.data("origin");
            if (!origin) {
                return true
            }
            to.removeData("origin");
            origin.data("isOrigin", false);
            this.getDrag = function() {
                return origin
            }
            ;
            var dropContainer = this.getDropContainer()
              , containerChanged = dropContainer.data("containerNumber") != origin.parent().data("containerNumber")
              , xChanged = to.data("positionX") != origin.data("positionX")
              , yChanged = to.data("positionY") != origin.data("positionY");
            temp.queue(function() {
                realDrag.remove();
                if (origin[0] != to[0]) {
                    origin.remove()
                }
                jQuery(this).dequeue()
            });
            return containerChanged || xChanged || yChanged
        }
        var from = this.getDrag()
          , amountOnStack = from.data("amount")
          , temp = from.clone().data("tooltip", "").css(this.getOriginalOffset()).css("z-index", 999).appendTo("body")
          , newOffset = this.getDropOffset()
          , type = this.getDropType()
          , originalAmountTo = to.data("amount");
        $(".image-grayed").removeClass("image-grayed");
        Tool.setAmount(to);
        Tool.setAmount(from, amountOnStack - amount);
        Tool.setAmount(temp, amount);
        temp.animate(newOffset, 500, function() {
            if (!Tool.inArray(type, ["enchant", "consume", "craft", "ninja"])) {
                Tool.setAmount(to, originalAmountTo + amount)
            }
            temp.hide()
        });
        this.temp = temp;
        if (!requestNeeded.call(this)) {
            to.data(this.getDrag().data());
            return true
        }
        var requestFunction = Tool.getFunctionReference(this.getDropContainer().data("requestFunction"));
        if (requestFunction) {
            temp.queue(function() {
                temp.remove().dequeue()
            });
            return requestFunction.call(this, to, amount)
        } else {
            this.sendRequest(to, amount, requestUrl);
            return false
        }
    }
    ;
    DropAction.prototype.callPostDragFunctions = function(subject, amount) {
        var removeFunc = Tool.getFunctionReference(this.getDragContainer().data("removeFunction"));
        if (amount > 0 && removeFunc) {
            removeFunc.call(this, subject, amount)
        }
        var appendFunc = Tool.getFunctionReference(this.getDropContainer().data("appendFunction"));
        if (amount > 0 && appendFunc && removeFunc != appendFunc) {
            appendFunc.call(this, subject, amount)
        }
    }
    ;
    function GridCell(contentType, map, x, y) {
        var w = map.length
          , h = map[0].length;
        function xFree(offset, oy) {
            if (null == oy) {
                oy = y
            }
            return (x + offset < w) && (x + offset >= 0) && !map[x + offset][oy]
        }
        function yFree(offset, ox) {
            ox = ox || x;
            return (y + offset < h) && (y + offset >= 0) && !map[ox][y + offset]
        }
        function checkSize16(xn, yn) {
            var lx = xn.length
              , ly = yn.length;
            for (var i = 0; i < lx; i++) {
                for (var j = 0; j < ly; j++) {
                    var dx = xn[i]
                      , dy = yn[j];
                    if (xFree(dx, y + dy)) {
                        return true
                    }
                }
            }
            return false
        }
        function checkSize32(xn, yn, yfn) {
            var l = yfn.length;
            for (var i = 0; i < l; i++) {
                var dy = yfn[i];
                if (Tool.inArray(-1, xn) && xFree(-1, y + dy) && xFree(-1, y + dy / 2)) {
                    return true
                }
                if (Tool.inArray(1, xn) && xFree(1, y + dy) && xFree(1, y + dy / 2)) {
                    return true
                }
            }
            if (yn.length == 2) {
                if (Tool.inArray(-1, xn) && xFree(-1, y + 1) && xFree(-1, y - 1)) {
                    return true
                }
                if (Tool.inArray(1, xn) && xFree(1, y + 1) && xFree(1, y - 1)) {
                    return true
                }
            }
            return false
        }
        this.accepts = function(drag) {
            var ct = drag.data("contentType") || 0;
            if (-1 == ct) {
                return true
            }
            if (!(ct & contentType)) {
                return false
            }
            var dragSize = drag.data("contentSize");
            if (1 == dragSize) {
                return true
            }
            var xNeighbors = []
              , yNeighbors = []
              , yFarNeighbors = [];
            if (xFree(-1)) {
                xNeighbors.push(-1)
            }
            if (xFree(1)) {
                xNeighbors.push(1)
            }
            if (yFree(-1)) {
                yNeighbors.push(-1);
                if (yFree(-2)) {
                    yFarNeighbors.push(-2)
                }
            }
            if (yFree(1)) {
                yNeighbors.push(1);
                if (yFree(2)) {
                    yFarNeighbors.push(2)
                }
            }
            var lx = xNeighbors.length
              , ly = yNeighbors.length;
            switch (dragSize) {
            case 2:
                return ly > 0;
            case 4:
                return lx > 0;
            case 8:
                return ly > 1 || yFarNeighbors.length > 0;
            case 16:
                return checkSize16(xNeighbors, yNeighbors);
            case 32:
                return checkSize32(xNeighbors, yNeighbors, yFarNeighbors);
            default:
                return false
            }
        }
        ;
        this.createDroppable = function() {
            var drop = $('<div class="ui-droppable grid-droparea">');
            drop.css({
                left: (x * 32) + "px",
                top: (y * 32) + "px",
                width: "32px",
                height: "32px"
            }).data("dropPositioning", new DropPositioning(map,x,y)).droppable(new Droppable(true));
            return drop
        }
    }
    function DropPositioning(map, x, y) {
        function canBeDropped(left, top, w, h) {
            if (left < 0 || top < 0 || left + w > map.length || top + h > map[0].length) {
                return false
            }
            for (var dx = 0; dx < w; dx++) {
                for (var dy = 0; dy < h; dy++) {
                    if (map[left + dx][top + dy]) {
                        return false
                    }
                }
            }
            return true
        }
        this.getClosestValidPosition = function(drop, ui) {
            var w = ui.draggable.data("measurementX")
              , h = ui.draggable.data("measurementY")
              , left = (ui.offset.left - drop.offset().left) / 32
              , top = (ui.offset.top - drop.offset().top) / 32
              , options = [];
            for (var dx = -1; dx < 1; dx++) {
                for (var dy = -2; dy < 2; dy++) {
                    if (canBeDropped(x + dx, y + dy, w, h)) {
                        options.push([dx, dy])
                    }
                }
            }
            var l = options.length;
            for (var i = 0; i < l; i++) {
                var cLeft = 32 * left
                  , cTop = 32 * top
                  , newLeft = 32 * (x + options[i][0])
                  , newTop = 32 * (y + options[i][1])
                  , dist = Math.sqrt(Math.pow(cLeft - newLeft, 2) + Math.pow(cTop - newTop, 2));
                options[i].push(dist)
            }
            options.sort(function sort(a, b) {
                return a[2] - b[2]
            });
            return {
                left: (x + options[0][0]) * 32,
                top: (y + options[0][1]) * 32
            }
        }
    }
    function DragProperties(drag, drop, noDestacking) {
        noDestacking = noDestacking || drop.data("noDestack") || drag.parent().data("noDestack");
        this.type = "move";
        if (-1 == drag.data("contentType")) {
            this.type = "ninja"
        } else {
            if (drop.is(".ui-draggable")) {
                this.type = "enchant";
                if (drop.data("hash") == drag.data("hash")) {
                    this.type = "stack"
                } else {
                    if (64 == drag.data("contentType") && 64 == drop.data("contentType")) {
                        this.type = "craft"
                    }
                }
            } else {
                if (Tool.getContainerType(drop) == Tool.CONTAINER_AVATAR) {
                    this.type = "consume"
                } else {
                    if (Tool.getContainerType(drop) == Tool.CONTAINER_EQUIPMENT) {
                        this.type = "equip"
                    }
                }
            }
        }
        this.drag = drag;
        this.drop = drop;
        this.noDestacking = !!noDestacking
    }
    DragProperties.prototype.getSoulbindRequired = function() {
        return ("equip" == this.type && !this.drag.data("soulboundTo"))
    }
    ;
    DragProperties.prototype.getAmount = function() {
        var drag = this.drag
          , drop = this.drop
          , type = this.type;
        this.amountAllowed = Math.min(drop.data("maxStackSize") || Infinity, drag.data("amount"));
        if (this.noDestacking) {
            return this.amountAllowed
        }
        if (drop.data("noCombine") || drop.data("noStack") || ("consume" == type) || ("equip" == type)) {
            this.amountAllowed = 1
        }
        if ("stack" == type) {
            this.amountAllowed = Math.min(100 - drop.data("amount"), this.amountAllowed)
        }
        if (1 == this.amountAllowed || controlState) {
            return 1
        }
        if ("enchant" == type || "craft" == type) {
            return drop.data("amount")
        }
        if ($("#moveStack").prop("checked")) {
            return this.amountAllowed
        }
        return null
    }
    ;
    var Tool = {
        CONTAINER_PACKAGE: 0,
        CONTAINER_AVATAR: 1,
        CONTAINER_EQUIPMENT: 2,
        CONTAINER_MERC: 3,
        CONTAINER_SHOP: 4,
        CONTAINER_STORAGE: 5,
        CONTAINER_ITEMBOX: 6,
        CONTAINER_INVENTORY: 7,
        CONTAINER_FORGE: 8,
        CONTAINER_UNKNOWN: 256
    };
    Tool.inArray = function(element, array) {
        return -1 < $.inArray(element, array)
    }
    ;
    Tool.getContainerType = function(cont) {
        var num = cont.data("container-number") || cont.parent().data("container-number");
        switch (true) {
        case 0 > num:
            return this.CONTAINER_PACKAGE;
        case 8 == num:
            return this.CONTAINER_AVATAR;
        case 2 <= num && num <= 11:
            return this.CONTAINER_EQUIPMENT;
        case 12 <= num && num <= 15:
            return this.CONTAINER_MERC;
        case 256 <= num && num <= 341:
            return this.CONTAINER_SHOP;
        case 352 <= num && num <= 357:
            return this.CONTAINER_STORAGE;
        case 383 <= num && num <= 385:
            return this.CONTAINER_ITEMBOX;
        case 512 <= num && num <= 521:
            return this.CONTAINER_INVENTORY;
        case 769 <= num && num <= 886:
            return this.CONTAINER_FORGE;
        default:
            return this.CONTAINER_UNKNOWN
        }
    }
    ;
    Tool.getFunctionReference = function(ref) {
        if ("string" == typeof (ref)) {
            try {
                if ("function" == eval("var a = " + ref + "; typeof a")) {
                    ref = a
                }
            } catch (err) {}
        }
        return ("function" == typeof ref) ? ref : null
    }
    ;
    Tool.setAmount = function(el, value) {
        if (null == value) {
            value = el.data("amount")
        } else {
            el.data("amount", value)
        }
        if (0 == value) {
            el.trigger("mouseleave.tooltip")
        }
        el.attr("data-amount", value)
    }
    ;
    Tool.buildDroppableMap = function(elements, w, h) {
        var map = new Array(w);
        for (var i = 0; i < w; i++) {
            map[i] = new Array(h)
        }
        elements.each(function() {
            var obj = $(this)
              , l = obj.data("positionX") - 1
              , t = obj.data("positionY") - 1
              , w = obj.data("measurementX")
              , h = obj.data("measurementY");
            for (var x = 0; x < w; x++) {
                for (var y = 0; y < h; y++) {
                    map[l + x][t + y] = this
                }
            }
        });
        return map
    }
    ;
    Tool.rebuildGridDropareas = function(el) {
        if (!el) {
            el = $("div.ui-droppable-grid")
        }
        if (!draggingElement) {
            return el
        }
        var drag = draggingElement;
        var gridSizes = [];
        $(el).each(function(i) {
            gridSizes[i] = {
                w: Math.round($(this).width() / 32),
                h: Math.round($(this).height() / 32)
            }
        });
        $(el).find(".grid-droparea").remove();
        $(el).each(function(i) {
            var drop = $(this);
            if (!drop.is(".ui-droppable-grid")) {
                return
            }
            var isStorage = Tool.CONTAINER_STORAGE == Tool.getContainerType(drop);
            if (isStorage && drag.data("questType")) {
                return
            }
            var w = gridSizes[i].w
              , h = gridSizes[i].h
              , map = Tool.buildDroppableMap(drop.children().not(drag), w, h);
            for (var x = 0; x < w; x++) {
                for (var y = 0; y < h; y++) {
                    var cell = new GridCell(drop.data("contentTypeAccept") || 0,map,x,y);
                    if (!map[x][y] && cell.accepts($(drag))) {
                        drop.prepend(cell.createDroppable())
                    }
                }
            }
        });
        return el
    }
    ;
    $(".ui-draggable", '[data-no-remove="true"]').each(function() {
        $(this).removeClass("ui-draggable")
    });
    window.DragDrop = {
        createDraggable: function(parent, data, spriteClass) {
            var el = $("<div>").data(data).addClass(spriteClass).appendTo(parent);
            this.makeDraggable(el);
            return el
        },
        makeDraggable: function(el, fixTooltipsAndInfo) {
            if (null == fixTooltipsAndInfo) {
                fixTooltipsAndInfo = true
            }
            $(el).each(function() {
                var el = $(this)
                  , cn = el.parent().data("containerNumber");
                if (cn) {
                    el.data("containerNumber", cn)
                }
                el.draggable(new Draggable());
                if (fixTooltipsAndInfo) {
                    tooltips.init(el);
                    itemInfo.init(el)
                }
            })
        },
        makeDroppable: function(el) {
            $(el).each(function() {
                var el = $(this);
                el.droppable(new Droppable(false,el.is(".ui-draggable")))
            })
        },
        refreshDroppables: function(el) {
            if (!draggingElement) {
                return
            }
            var drag = draggingElement;
            el = Tool.rebuildGridDropareas(el);
            el.find(".ui-droppable").not(drag).each(function() {
                if (0 == $(this).data("amount")) {
                    return
                }
                if ($(this).data("ui-droppable").accept.call(this, drag)) {
                    $(this).addClass("active")
                } else {
                    $(this).addClass("image-grayed")
                }
            });
            drag.on("drag.positionRefresh", function() {
                var refresh = drag.draggable("option", "refreshPositions");
                drag.draggable("option", "refreshPositions", !refresh);
                if (refresh) {
                    drag.off("drag.positionRefresh")
                }
            })
        },
        getDraggingElement: function() {
            return draggingElement
        }
    };
    DragDrop.makeDraggable($(".ui-draggable"), false);
    DragDrop.makeDroppable($(".ui-droppable"))
});
;(function(a) {
    a.widget("gf.layer", {
        options: {
            type: "thin",
            draggable: false,
            fixed: false,
            position: null,
            title: null,
            autoOpen: true,
            effect: "fade",
            closeButton: true,
            modal: false
        },
        _create: function() {
            this._cachedElement = this.element.clone();
            var c = this.options.title || a(this.element).attr("title");
            var d = a(this.element).html();
            this.element.addClass("gf-widget-layout").empty().removeAttr("title");
            if (this.options.modal) {
                a("<div>").addClass("gf-layer-overlay").overlay().appendTo(this.element).bind("click", a.proxy(this._triggerClose, this))
            }
            var b = a("<div>").addClass("gf-layer gf-layer-" + this.options.type).appendTo(this.element);
            if (this.options.draggable) {
                this.element.children(".gf-layer").css("cursor", "move").draggable()
            }
            if (this.options.fixed) {
                b.css("position", "fixed")
            }
            this.topElement = a("<div>").addClass("gf-layer-top").appendTo(this.element.children(".gf-layer"));
            this.middleElement = a("<div>").addClass("gf-layer-middle").appendTo(this.element.children(".gf-layer"));
            this.bottomElement = a("<div>").addClass("gf-layer-bottom").appendTo(this.element.children(".gf-layer"));
            a("<a>").addClass("gf-widget-close").bind("click", a.proxy(this._triggerClose, this)).appendTo(this.topElement);
            a("<div>").addClass("gf-layer-title").text(c).appendTo(this.element.children(".gf-layer").find(".gf-layer-middle"));
            a("<div>").addClass("gf-layer-description").html(d).appendTo(this.element.children(".gf-layer").find(".gf-layer-middle"));
            if (this.options.autoOpen) {
                this.open("slow")
            }
        },
        _triggerClose: function() {
            this.close()
        },
        destroy: function() {
            this._cachedElement.after(this.element);
            this.element.remove();
            a.Widget.prototype.destroy.apply(this, arguments)
        },
        open: function(c) {
            var b = a.Deferred();
            this.element.children(".gf-layer-overlay").overlay("show");
            this.element.children(".gf-layer").show(this.options.effect, c, a.proxy(function() {
                b.resolve("open")
            }, this)).position(a.extend({
                of: ".gf-attach-element"
            }, this.options.position || {}));
            this._trigger("open", null, [b])
        },
        close: function(c) {
            var b = a.Deferred();
            this.element.children(".gf-layer-overlay").overlay("hide");
            this.element.children(".gf-layer").hide(this.options.effect, c, a.proxy(function() {
                b.resolve("close")
            }, this));
            this._trigger("close", null, [b])
        }
    })
}
)(jQuery);
;(function(a) {
    a.widget("gf.overlay", {
        options: {
            duration: 250,
            hideOpacity: 0,
            showOpacity: 0.7
        },
        _create: function() {
            this.element.addClass("gf-overlay").css("opacity", this.options.hideOpacity).height(a(document).height());
            a(window).bind("resize", a.proxy(function() {
                a(this).width(a(document).width());
                a(this).height(a(document).height())
            }, this))
        },
        destroy: function() {
            this.element.hide();
            this.element.remove();
            a.Widget.prototype.destroy.apply(this, arguments)
        },
        show: function() {
            this.element.fadeTo(this.options.duration, this.options.showOpacity)
        },
        hide: function() {
            this.element.fadeTo(this.options.duration, this.options.hideOpacity)
        }
    })
}
)(jQuery);
;function isFunction(a) {
    var b = {};
    return a && b.toString.call(a) === "[object Function]"
}
var Header = new Class({
    TT_ELEMENTS: {
        expedition: "expeditionpoints",
        dungeon: "dungeonpoints",
        arena: "arena",
        ct: "grouparena"
    },
    fade: function(a, c, b) {
        if (jQuery(a).text() != c) {
            this.doFade(jQuery(a).text(c), b)
        }
    },
    doFade: function(a, b) {
        jQuery(a).effect("bounce").parent(".ui-effects-wrapper").css("display", "inline-block");
        if (isFunction(b)) {
            b(this)
        }
    },
    updateStatus: function(b, c) {
        var d = jQuery("#menue_" + b);
        if (c.count > 0) {
            d.removeClass("menue_" + b);
            d.addClass("menue_" + b + "_highlight")
        } else {
            d.addClass("menue_" + b);
            d.removeClass("menue_" + b + "_highlight")
        }
        d.attr("href", c.link);
        var a = jQuery("#menue_" + b + " .menue_new_count");
        if (c.count > 0) {
            if (!a.length) {
                a = jQuery("<div>").addClass("menue_new_count").appendTo(d)
            }
            this.fade(a, (c.count > 99 ? "+" : c.count))
        } else {
            a.remove()
        }
    },
    updateNews: function(a) {
        this.updateStatus("news", a)
    },
    updateReports: function(a) {
        this.updateStatus("reports", a)
    },
    updateMessages: function(a) {
        this.updateStatus("messages", a)
    },
    updatePackages: function(a) {
        this.updateStatus("packages", a)
    },
    getGold: function() {
        return $("sstat_gold_val").innerHTML.replace(/\./g, "").toInt()
    },
    updateGoldHook: function(a) {
        if (a.getGold() >= 1000000000) {
            jQuery("#sstat_gold_val").removeClass("headervalue_small").addClass("headervalue_smaller")
        } else {
            jQuery("#sstat_gold_val").removeClass("headervalue_smaller").addClass("headervalue_small")
        }
    },
    updateGold: function(a) {
        this.fade("#sstat_gold_val", a.text, this.updateGoldHook)
    },
    getRubies: function() {
        return $("sstat_ruby_val").innerHTML.replace(/\./g, "").toInt()
    },
    updateRubiesHook: function(a) {
        if (a.getRubies() >= 10000) {
            jQuery("#sstat_ruby_val").removeClass("headervalue_small").addClass("headervalue_smaller")
        } else {
            jQuery("#sstat_ruby_val").removeClass("headervalue_smaller").addClass("headervalue_small")
        }
    },
    updateRubies: function(a) {
        this.fade("#sstat_ruby_val", a.text, this.updateRubiesHook)
    },
    updateHighscore: function(a) {
        this.fade("#highscorePlace", a.value);
        tooltips.set("#icon_highscore", a.tooltip)
    },
    updateLevel: function(a) {
        this.fade("#header_values_level", a)
    },
    updateHealth: function(c) {
        var a = jQuery("#header_values_hp_bar");
        tooltips.set(a, c.tooltip);
        var b = Math.floor(100 * c.value / c.maxValue);
        this.fade("#header_values_hp_percent", b + "%", function() {
            a.data(c);
            a.trigger("restart")
        })
    },
    updateExperience: function(a) {
        tooltips.set("#header_values_xp_bar", a.tooltip);
        jQuery("#header_values_xp_bar_fill").animate({
            width: a.percentage + "%"
        }, "slow");
        this.fade("#header_values_xp_percent", a.percentage + "%")
    },
    updateLocations: function(a, b) {
        this.fade("#" + a + "points_value_point", b.points);
        this.fade("#" + a + "points_value_pointmax", b.pointsMax)
    },
    updateActions: function(e, f, c, g) {
        var b = f.cooldown.start != g.start
          , h = f.cooldown.end != g.end
          , a = c ^ ("-" == g.readyText);
        if (b || h || a) {
            this.doFade("#cooldown_bar_text_" + e)
        }
        g.readyText = c ? "-" : f.text;
        g.start = f.cooldown.start;
        g.now = f.cooldown.time;
        g.end = f.cooldown.end;
        g.update();
        if (f.color === null) {
            jQuery("#cooldown_bar_text_dungeon").css("color", "")
        } else {
            jQuery("#cooldown_bar_text_dungeon").css("color", f.color)
        }
        var d = this.TT_ELEMENTS[e];
        tooltips.set("#icon_" + d, f.tooltip);
        jQuery("#cooldown_bar_" + e + " a").attr("href", f.link)
    },
    updateExpedition: function(b, a) {
        this.updateLocations("expedition", b);
        this.updateActions("expedition", b, a, expeditionProgressBar)
    },
    updateDungeon: function(b, a) {
        this.updateLocations("dungeon", b);
        this.updateActions("dungeon", b, a, dungeonProgressBar)
    },
    updateArena: function(b, a) {
        this.updateActions("arena", b, a, arenaProgressBar);
        this.fade("#arenaPlace", b.value)
    },
    updateGrouparena: function(b, a) {
        this.updateActions("ct", b, a, ctProgressBar);
        this.fade("#grouparenaPlace", b.value)
    },
    update: function(a) {
        if (!a.header) {
            return this
        }
        if (a.header.newNews) {
            this.updateNews(a.header.newNews)
        }
        if (a.header.newReports) {
            this.updateReports(a.header.newReports)
        }
        if (a.header.newMessages) {
            this.updateMessages(a.header.newMessages)
        }
        if (a.header.newPackages) {
            this.updatePackages(a.header.newPackages)
        }
        if (a.header.gold) {
            this.updateGold(a.header.gold)
        }
        if (a.header.rubies) {
            this.updateRubies(a.header.rubies)
        }
        if (a.header.highscore) {
            this.updateHighscore(a.header.highscore)
        }
        if (a.header.level) {
            this.updateLevel(a.header.level)
        }
        if (a.header.health) {
            this.updateHealth(a.header.health)
        }
        if (a.header.experience) {
            this.updateExperience(a.header.experience)
        }
        if (a.header.expedition) {
            this.updateExpedition(a.header.expedition, !!a.header.actionsBlocked)
        }
        if (a.header.dungeon) {
            this.updateDungeon(a.header.dungeon, !!a.header.actionsBlocked)
        }
        if (a.header.arena) {
            this.updateArena(a.header.arena, !!a.header.actionsBlocked)
        }
        if (a.header.grouparena) {
            this.updateGrouparena(a.header.grouparena, !!a.header.actionsBlocked)
        }
    }
});
jQuery(function() {
    var d = jQuery
      , c = d("#header_values_hp_bar")
      , a = d("#header_values_hp_percent");
    function b(e) {
        this.duration = e;
        this.easing = "linear";
        this.step = function(f) {
            a.text(Math.floor(f) + "%")
        }
    }
    c.on("restart", function(i) {
        var h = c.data("value")
          , k = c.data("maxValue")
          , g = c.data("regenPerHour")
          , f = Math.floor(100 * h / k)
          , e = k - h
          , l = 3600000 * e / g
          , j = i.data ? 0 : "slow";
        d(this).find("div").stop().animate({
            width: f + "%"
        }, j).animate({
            width: "100%"
        }, new b(l))
    });
    c.trigger("restart", true)
});
var headerObject = new Header();
;jQuery(function() {
    var a = jQuery;
    function c(d, f) {
        this.cont = d;
        this.nav = f;
        this.currentBag = f.find(".current").data("bagNumber");
        this.bagContents = [];
        this.bagLoaded = false;
        this.spinner = a('<div class="spinner-img"></div>');
        d.append(this.spinner);
        this.spinner.css({
            position: "absolute",
            left: (d.width() - this.spinner.width()) / 2,
            top: (d.height() - this.spinner.height()) / 2
        });
        var e = this;
        a(document).on("dragstart", function(g) {
            f.children().each(function() {
                if (a(this).data("available")) {
                    a(this).addClass("active")
                }
            })
        });
        a(document).on("dragend", function() {
            f.children().removeClass("active")
        });
        f.on("click", "a", function() {
            e.openBag(a(this));
            var g = DragDrop.getDraggingElement();
            if (!g) {
                a(".image-grayed").removeClass("image-grayed")
            }
        });
        f.on("mouseenter", "a", function() {
            var g = DragDrop.getDraggingElement();
            if (!g) {
                return
            }
            var h = a(this);
            var i = window.setTimeout(function() {
                if (!DragDrop.getDraggingElement()) {
                    return
                }
                h.click();
                DragDrop.refreshDroppables(d);
                var k = g.offset();
                if (!g.parent().length) {
                    var j = f.find("a").filter(function() {
                        return a(this).data("bagNumber") == g.data("containerNumber")
                    });
                    k = j.offset();
                    k.opacity = 0;
                    k.left += (j.width() - 32 * g.data("measurementX")) / 2;
                    k.top += (j.height() - 32 * g.data("measurementY")) / 2
                }
                g.data("ui-draggable").originalPosition = k
            }, 250);
            h.on("mouseleave", function() {
                window.clearTimeout(i);
                h.off("mouseleave")
            })
        });
        this.loadQueue = f.children().toArray();
        this.loadBag(f.find(".current"))
    }
    c.prototype.loadBag = function(f) {
        var d = this.loadQueue.indexOf(f[0]);
        if (d > -1) {
            delete this.loadQueue[d];
            var e = f.data("bagNumber")
              , g = {
                mod: "inventory",
                submod: "loadBag",
                bag: e,
                shopType: this.cont.data("shopType")
            };
            sendAjax(a("<dummy>"), "ajax.php", a.param(g), this.createCallback(f), error)
        }
    }
    ;
    c.prototype.createCallback = function(e) {
        var d = this;
        return function(h) {
            var f = e.data("bagNumber")
              , g = e.data("available");
            h = a(h);
            if (g) {
                DragDrop.makeDraggable(h);
                DragDrop.makeDroppable(h)
            }
            d.bagContents[f] = h;
            if (f == d.currentBag) {
                d.openBag(e)
            }
            a.each(d.loadQueue, function() {
                d.loadBag(a(this))
            })
        }
    }
    ;
    c.prototype.openBag = function(f) {
        if (this.bagLoaded) {
            this.cont.find(".grid-droparea").remove();
            this.bagContents[this.currentBag] = this.cont.children();
            var d = this;
            this.bagContents[this.currentBag].on("remove", {
                bag: this.currentBag
            }, function(i) {
                var h = this;
                d.bagContents[i.data.bag] = d.bagContents[i.data.bag].filter(function() {
                    return this != h
                })
            })
        } else {
            this.loadBag(f)
        }
        var e = f.data("bagNumber");
        f.addClass("current").siblings().removeClass("current");
        var g = {
            containerNumber: e
        };
        this.bagLoaded = !!this.bagContents[e];
        this.cont.children().detach();
        this.cont.append(this.bagContents[e] || this.spinner).data(g).children().off("remove").data(g);
        if (this.openCallback) {
            this.openCallback.call(this, e, f)
        }
        this.currentBag = e
    }
    ;
    c.prototype.onOpen = function(d) {
        this.openCallback = d
    }
    ;
    var b = new c(a("#inv"),a("#inventory_nav"));
    b.onOpen(function(g, f) {
        if (g != this.currentBag) {
            a.ajax("ajax.php?mod=inventory&submod=setInv&inv=" + (g - 512) + "&sh=" + secureHash)
        }
        var h = this.bagLoaded && f.data("available");
        this.cont.data("noRemove", !h).removeClass("ui-droppable-grid unavailable").addClass(h ? "ui-droppable-grid" : "unavailable");
        var i = this.cont.parent().find(".bag_buy_extend")
          , e = f.data("extendMessage")
          , d = f.data("extendLink");
        i.css("color", h ? "green" : "red");
        i.children().text("").off("click");
        if (e) {
            i.find("span").html(e)
        }
        if (d) {
            i.find("a").html(d.text).click(function() {
                blackoutDialogFlex("window.location.href = '" + d.link + "'", "", true, d.popup)
            })
        }
    })
});
;(function(b) {
    b(function() {
        function c(f) {
            var e = b("#content");
            e.removeClass("show-item-quality show-item-level");
            if (f > 0) {
                e.addClass("show-item-quality")
            }
            if (f > 1) {
                e.addClass("show-item-level")
            }
            localStorage.setItem("showItemInfo", f)
        }
        var d = (localStorage.getItem("showItemInfo") || "2");
        c(d);
        b("#show-item-info").find("input").click(function() {
            c(b(this).val())
        }).each(function() {
            b(this).prop("checked", d === b(this).val())
        })
    });
    function a(c) {
        c = b(c);
        b(c).attr({
            "data-quality": c.data("quality"),
            "data-level": c.data("level")
        })
    }
    window.itemInfo = {
        init: a
    }
}
)(jQuery);
;(function(a) {
    a.widget("gf.buff", {
        options: {
            width: 38,
            height: 38,
            delay: 1000,
            lang: {},
            debug: false
        },
        _init: function() {
            this._instance = a.gf.buff._counter != undefined ? a.gf.buff._counter.length : 0;
            if (a.gf.buff._counter === undefined) {
                a.gf.buff._counter = []
            }
            if (a.gf.buff._counter[this._instance] === undefined) {
                a.gf.buff._counter[this._instance] = this._instance + 1
            }
            this._coords = [this.options.width / 2, this.options.height / 2];
            this._radius = Math.sqrt(Math.pow(this._coords[0], 2) + Math.pow(this._coords[1], 2));
            this._castStart = parseInt(this.element.data("castStart"));
            this._effectStart = parseInt(this.element.data("effectStart"));
            this._effectEnd = parseInt(this.element.data("effectEnd"));
            this._cooldownEnd = parseInt(this.element.data("cooldownEnd"));
            this._type = this.element.data("buffType");
            this._name = this.element.attr("title");
            this._canvas = this.element.children("canvas").get(0);
            this._context = this._canvas.getContext("2d");
            this._image = this.element.data("image")
        },
        _runInterval: function() {
            if (this.options.debug) {
                console.count("interval (" + a.gf.buff._counter[this._instance] + ") #")
            }
            this.element.buff("update");
            this.element.parent().delay(this.options.delay, "buffloop").queue("buffloop", a.proxy(this._runInterval, this)).dequeue("buffloop")
        },
        startInterval: function() {
            this.element.attr("title", "");
            this._runInterval()
        },
        stopInterval: function() {
            this.element.parent().clearQueue("buffloop")
        },
        _secondsToCountdown: function(e) {
            var f = 0
              , b = 0
              , c = 0
              , d = "";
            if (e >= 86400) {
                f = Math.floor(e / 86400);
                e = e % 86400
            }
            if (e >= 3600) {
                b = Math.floor(e / 3600);
                e = e % 3600
            }
            if (e >= 60) {
                c = Math.floor(e / 60);
                e = e % 60
            }
            if (f > 0) {
                d += f.toString() + " " + (this.options.lang[f !== 1 ? "DAYS" : "DAY"]) + " "
            }
            d += (b < 10 ? "0" : "") + b + ":" + (c < 10 ? "0" : "") + c + ":" + (e < 10 ? "0" : "") + e;
            return d
        },
        _drawArc: function(e, d, b, c) {
            if (c && b < 1 || !c && b > 0) {
                e.fillStyle = d;
                if (c && b > 0 || !c && b < 1) {
                    e.beginPath();
                    e.moveTo(this._coords[0], this._coords[1]);
                    e.arc(this._coords[0], this._coords[1], this._radius, Math.PI * (-0.5), Math.PI * (-0.5 + 2 * b), c);
                    e.lineTo(this._coords[0], this._coords[1]);
                    e.closePath();
                    e.fill()
                } else {
                    e.fillRect(0, 0, this.options.width, this.options.height)
                }
            }
        },
        _drawOctagon: function(c, b) {
            c.strokeStyle = "rgba(0, 0, 0, 1)";
            c.beginPath();
            c.moveTo(11.5, 1.5);
            c.lineTo(25.5, 1.5);
            c.lineTo(36.5, 11.5);
            c.lineTo(36.5, 25.5);
            c.lineTo(25.5, 36.5);
            c.lineTo(11.5, 36.5);
            c.lineTo(1.5, 25.5);
            c.lineTo(1.5, 11.5);
            c.closePath();
            c.stroke();
            c.strokeStyle = b;
            c.beginPath();
            c.moveTo(11.5, 2.5);
            c.lineTo(25.5, 2.5);
            c.lineTo(35.5, 11.5);
            c.lineTo(35.5, 25.5);
            c.lineTo(25.5, 35.5);
            c.lineTo(11.5, 35.5);
            c.lineTo(2.5, 25.5);
            c.lineTo(2.5, 11.5);
            c.closePath();
            c.stroke()
        },
        _removeOuterOctagon: function(b) {
            canvas = this.element.children("canvas").get(0);
            b = canvas.getContext("2d");
            b.save();
            b.globalCompositeOperation = "destination-out";
            b.fillStyle = "rgba(0, 0, 0, 1)";
            b.beginPath();
            b.moveTo(-1.5, -1.5);
            b.lineTo(13.5, -1.5);
            b.lineTo(-1.5, 13.5);
            b.rect(-1.5, -1.5, 39.5, 2.5);
            b.moveTo(24.5, -1.5);
            b.lineTo(39.5, -1.5);
            b.lineTo(39.5, 13.5);
            b.moveTo(39.5, 24.5);
            b.lineTo(39.5, 39.5);
            b.lineTo(23.5, 39.5);
            b.rect(-1.5, 36.5, 39.5, 39.5);
            b.moveTo(13.5, 39.5);
            b.lineTo(-1.5, 39.5);
            b.lineTo(-1.5, 23.5);
            b.rect(-1.5, -1.5, 2.5, 35.5);
            b.closePath();
            b.fill();
            b.restore()
        },
        _tooltip: function(c) {
            a("#buffListToolTip").remove();
            var b = a("<table id='buffListToolTip' cellspacing=2 cellpadding=2 valign=middle class='tooltipBox' style='position:absolute; left: " + currentMousePos.x + "px; top: " + currentMousePos.y + "px; z-index:1000;'></table>").append(a("<tr></tr>")).append(a("<td style='color:white; font-weight: bold; font-size:9pt' colspan='2' nowrap='nowrap'>" + c + "</td>"));
            a("body").append(b)
        },
        update: function() {
            var b, d, c;
            this._context.globalCompositeOperation = "source-over";
            if (this._cooldownEnd >= 0) {
                this._context.clearRect(0, 0, this.options.width, this.options.height);
                c = new Image();
                c.src = this._image;
                this._context.fillStyle = "rgba(201, 174, 121, 1)";
                this._context.rect(0, 0, 37, 37);
                this._context.fill();
                this._context.drawImage(c, 0, 0);
                if (this._castStart <= 0 && this._effectStart >= 0) {
                    b = (this._castStart * -1) + this._effectStart;
                    d = (this._castStart * -1) / b;
                    this._drawArc(this._context, "rgba(0, 0, 0, 0.5)", d, false);
                    this._drawArc(this._context, "rgba(220, 80, 80, 0.5)", d, false);
                    this.element.attr("onmousemove", 'showBuffListTooltip("' + this._name + " - " + this.options.lang.CAST_TIME + " " + this._secondsToCountdown(this._effectStart) + '")');
                    this.element.attr("onmouseout", "hideBuffListTooltip()")
                } else {
                    if (this._effectEnd >= 0) {
                        b = (this._effectStart * -1) + this._effectEnd;
                        d = (this._effectStart * -1) / b;
                        this._drawArc(this._context, "rgba(0, 0, 0, 0.5)", d, false);
                        this.element.attr("onmousemove", 'showBuffListTooltip("' + this._name + " - " + this._secondsToCountdown(this._effectEnd) + '")');
                        this.element.attr("onmouseout", 'hideBuffListTooltip("' + this._name + " - " + this._secondsToCountdown(this._effectEnd) + '")');
                        if (this._effectEnd === 0) {
                            PremiumSubject.activeElapsed(this._type)
                        }
                    } else {
                        b = (this._effectEnd * -1) + this._cooldownEnd;
                        d = (this._effectEnd * -1) / b;
                        this._drawArc(this._context, "rgba(0, 0, 0, 0.5)", d, true);
                        this._drawArc(this._context, "rgba(108, 147, 172, 0.5)", d, false);
                        this.element.attr("onmousemove", 'showBuffListTooltip("' + this._name + " - " + this.options.lang.COOLDOWN + " " + this._secondsToCountdown(this._cooldownEnd) + '")');
                        this.element.attr("onmouseout", "hideBuffListTooltip()")
                    }
                }
                this._removeOuterOctagon();
                this._drawOctagon(this._context, "rgba(254, 192, 70, 1)");
                this._effectStart--;
                this._effectEnd--;
                this._cooldownEnd--
            } else {
                PremiumSubject.cooldownElapsed(this._type);
                this.element.buff("destroy").remove()
            }
        },
        clone: function() {
            var b = this.element.clone(false);
            b.attr("title", this._name).buff(this.options);
            return b
        },
        destroy: function() {
            this.element.attr("title", this._name);
            a.Widget.prototype.destroy.apply(this, arguments)
        }
    })
}
)(jQuery);
var PremiumSubject = {
    observers: [],
    registerObserver: function(a) {
        PremiumSubject.observers.push(a)
    },
    activeElapsed: function(b) {
        for (var a = 0; a < PremiumSubject.observers; a++) {
            PremiumSubject.observers[a].activeElapsed(b)
        }
    },
    cooldownElapsed: function(b) {
        for (var a = 0; a < PremiumSubject.observers; a++) {
            PremiumSubject.observers[a].cooldownElapsed(b)
        }
    }
};
function showBuffListTooltip(b) {
    jQuery("#buffListToolTip").remove();
    var a = jQuery("<table id='buffListToolTip' cellspacing=2 cellpadding=2 valign=middle class='tooltipBox' style='position:absolute; left: " + (currentMousePos.x + 10) + "px; top: " + (currentMousePos.y + 10) + "px; z-index:1000;'></table>").append(jQuery("<tr></tr>")).append(jQuery("<td style='color:white; font-weight: bold; font-size:9pt' colspan='2' nowrap='nowrap'>" + b + "</td>"));
    jQuery("body").append(a)
}
function hideBuffListTooltip() {
    jQuery("#buffListToolTip").remove()
}
;;if (typeof JSON !== "object") {
    JSON = {}
}
(function() {
    function f(e) {
        return e < 10 ? "0" + e : e
    }
    function quote(e) {
        escapable.lastIndex = 0;
        return escapable.test(e) ? '"' + e.replace(escapable, function(e) {
            var t = meta[e];
            return typeof t === "string" ? t : "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
        }) + '"' : '"' + e + '"'
    }
    function str(e, t) {
        var n, r, i, s, o = gap, u, a = t[e];
        if (a && typeof a === "object" && typeof a.toJSON === "function") {
            a = a.toJSON(e)
        }
        if (typeof rep === "function") {
            a = rep.call(t, e, a)
        }
        switch (typeof a) {
        case "string":
            return quote(a);
        case "number":
            return isFinite(a) ? String(a) : "null";
        case "boolean":
        case "null":
            return String(a);
        case "object":
            if (!a) {
                return "null"
            }
            gap += indent;
            u = [];
            if (Object.prototype.toString.apply(a) === "[object Array]") {
                s = a.length;
                for (n = 0; n < s; n += 1) {
                    u[n] = str(n, a) || "null"
                }
                i = u.length === 0 ? "[]" : gap ? "[\n" + gap + u.join(",\n" + gap) + "\n" + o + "]" : "[" + u.join(",") + "]";
                gap = o;
                return i
            }
            if (rep && typeof rep === "object") {
                s = rep.length;
                for (n = 0; n < s; n += 1) {
                    if (typeof rep[n] === "string") {
                        r = rep[n];
                        i = str(r, a);
                        if (i) {
                            u.push(quote(r) + (gap ? ": " : ":") + i)
                        }
                    }
                }
            } else {
                for (r in a) {
                    if (Object.prototype.hasOwnProperty.call(a, r)) {
                        i = str(r, a);
                        if (i) {
                            u.push(quote(r) + (gap ? ": " : ":") + i)
                        }
                    }
                }
            }
            i = u.length === 0 ? "{}" : gap ? "{\n" + gap + u.join(",\n" + gap) + "\n" + o + "}" : "{" + u.join(",") + "}";
            gap = o;
            return i
        }
    }
    if (typeof Date.prototype.toJSON !== "function") {
        Date.prototype.toJSON = function() {
            return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
        }
        ;
        String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function() {
            return this.valueOf()
        }
    }
    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, gap, indent, meta = {
        "\b": "\\b",
        "    ": "\\t",
        "\n": "\\n",
        "\f": "\\f",
        "\r": "\\r",
        '"': '\\"',
        "\\": "\\\\"
    }, rep;
    if (typeof JSON.stringify !== "function") {
        JSON.stringify = function(e, t, n) {
            var r;
            gap = "";
            indent = "";
            if (typeof n === "number") {
                for (r = 0; r < n; r += 1) {
                    indent += " "
                }
            } else {
                if (typeof n === "string") {
                    indent = n
                }
            }
            rep = t;
            if (t && typeof t !== "function" && (typeof t !== "object" || typeof t.length !== "number")) {
                throw new Error("JSON.stringify")
            }
            return str("", {
                "": e
            })
        }
    }
    if (typeof JSON.parse !== "function") {
        JSON.parse = function(text, reviver) {
            function walk(e, t) {
                var n, r, i = e[t];
                if (i && typeof i === "object") {
                    for (n in i) {
                        if (Object.prototype.hasOwnProperty.call(i, n)) {
                            r = walk(i, n);
                            if (r !== undefined) {
                                i[n] = r
                            } else {
                                delete i[n]
                            }
                        }
                    }
                }
                return reviver.call(e, t, i)
            }
            var j;
            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function(e) {
                    return "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
                })
            }
            if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) {
                j = eval("(" + text + ")");
                return typeof reviver === "function" ? walk({
                    "": j
                }, "") : j
            }
            throw new SyntaxError("JSON.parse")
        }
    }
}
)();
;function error(d, e, b) {
    var c = jQuery('<div id="ajaxErrorDialog" class="error" />').text(e.responseText).css("position", "absolute").hide().appendTo("body");
    var a = jQuery(d).offset();
    if (b === undefined) {
        b = 2000
    }
    c.css({
        top: a.top,
        width: "500px",
        left: a.left
    }).fadeIn().delay(b).fadeOut()
}
function requestDeleteMessage(b, a) {
    jQuery("#message" + a).fadeOut();
    sendAjax(b, "ajax.php", "mod=messages&submod=deleteMessage&messageId=" + a, messageActionDone, error)
}
function messageActionDone(b) {
    b = JSON.parse(b);
    headerObject.update(b);
    if (!b.succeed) {
        var a = jQuery("#message" + b.messageId);
        a.fadeIn();
        error(a, b, 5000)
    }
}
function requestMoveMessage(c) {
    var a = c.data.messageId;
    var d = c.data.folderId;
    var b = jQuery("#message" + a);
    b.fadeOut();
    sendAjax(b, "ajax.php", "mod=messages&submod=moveMessage&messageId=" + a + "&folderId=" + d, messageActionDone, error)
}
$.noConflict();
(function(a) {
    a(document).ready(function() {
        a(".buff-container").bind("click", function(c) {
            var b = a(c.target);
            if (b.is("canvas")) {
                b = b.parent("div.buff")
            }
            if (b.hasClass("buff-clickable")) {
                window.location.href = b.data("link")
            }
        });
        a(".buff-many-opener").bind("click", function() {
            var b = a("<div>").attr("title", lang.SHOW_ACTIVE_BUFFS).layer({
                position: {
                    of: "#mainnav",
                    my: "top"
                },
                autoOpen: false,
                modal: true
            }).bind("layerclose", function() {
                a(this).children().unbind("remove");
                a(this).unbind("remove").remove()
            }).appendTo("body");
            var c = a(document.createElement("div")).addClass("buff-container");
            a(this).prev().children().each(function(d, e) {
                if (e.hasClass("buff")) {
                    a(e).buff("clone").appendTo(c)
                }
            });
            c.addClass("clearfix").css({
                width: "205px",
                marginLeft: "-2px"
            }).appendTo(b.find(".gf-layer-description"));
            c.find(".buff").buff("startInterval");
            b.layer("open")
        });
        a(".message_icon").on("click", function() {
            a(this).parent().parent().next().fadeToggle()
        });
        a(".message_box_title.toggleable").on("click", function() {
            a(this).parent().next().fadeToggle();
            var c = a(this).data("toggleId")
              , b = {
                spinnerVisible: false
            };
            sendAjax(a(this), "ajax.php", "mod=messages&submod=toggleMessage&messageId=" + c, null, null, b)
        });
        a(".moveMessage").on("mouseover", function() {
            a(this).append(a("#moveToFolderDiv"));
            mid = a(this).data("mid");
            a(".moveToFolderLink").each(function() {
                fid = a(this).data("folder");
                a(this).off().on("click", {
                    messageId: mid,
                    folderId: fid
                }, requestMoveMessage)
            })
        });
        a("#show-previous-message").click(function() {
            var c = a(this)
              , b = c.data("nid")
              , d = c.parent().fadeOut();
            sendAjax(d.parent(), "ajax.php", "mod=messages&submod=loadMessage&messageId=" + b, function(e) {
                a(e.message).hide().insertBefore(d).fadeIn();
                if (e.previousId) {
                    c.data("nid", e.previousId);
                    d.stop().fadeIn()
                } else {
                    d.stop(true, true)
                }
            }, null, {
                dataType: "JSON"
            })
        })
    })
}
)(jQuery);
jQuery(window).bind("load", function() {
    jQuery(".buff").buff({
        width: 38,
        height: 38,
        lang: lang
    }).buff("startInterval")
});
;function onMouseHover(b, a) {
    document.getElementById(b).className = a
}
function setFocus(c, b) {
    if (!b && document.activeElement.nodeName != document.body.nodeName) {
        return
    }
    var a = $(c);
    if (a.get("tag") != "form") {
        a.focus();
        return
    }
    if (a.elements[0] != null) {
        for (var d = 0; d < a.length; d++) {
            if (a.elements[d].type != "hidden" && !a.elements[d].disabled && !a.elements[d].readOnly) {
                a.elements[d].focus();
                break
            }
        }
    }
}
function addCharCounter(b, c) {
    var a = updateCharCounter.pass([b, c]);
    $(b).addEvent("keyup", a);
    Function.attempt(a)
}
function updateCharCounter(a, b) {
    $(b).set("text", $(a).value.length)
}
var SlideElementToggle = new Class({
    slideElement: null,
    slideElementId: null,
    toggleElementId: null,
    toggleClassesArray: null,
    initialize: function(c, b) {
        this.toggleClassesArray = [];
        this.slideElementId = c;
        this.toggleElementId = b;
        this.slideElement = new Fx.Slide(this.slideElementId);
        var a = this;
        $(this.toggleElementId).addEvent("click", function() {
            a.toggleAllClasses(a.slideElement.open);
            a.slideElement.toggle()
        });
        this.slideElement.hide()
    },
    addToggleClass: function(b, c, d) {
        var a = this;
        a.toggleClassesArray.push([b, c, d]);
        this.toggleAllClassesToClosed()
    },
    toggleAllClasses: function(d) {
        var a = this;
        for (var c = 0; c < a.toggleClassesArray.length; c++) {
            var b = a.toggleClassesArray[c][0];
            var e = a.toggleClassesArray[c][1];
            var f = a.toggleClassesArray[c][2];
            if (d) {
                $(b).removeClass(e);
                $(b).addClass(f)
            } else {
                $(b).removeClass(f);
                $(b).addClass(e)
            }
        }
    },
    toggleAllClassesToClosed: function() {
        this.toggleAllClasses(true)
    },
    toggleAllClassesToOpen: function() {
        this.toggleAllClasses(false)
    },
    show: function() {
        this.slideElement.show();
        this.toggleAllClassesToOpen()
    },
    hide: function() {
        this.slideElement.hide();
        this.toggleAllClassesToClosed()
    }
});
function toggleCostume(d, e, c, b) {
    e = JSON.decode(e);
    c = JSON.decode(c);
    var a = function(g, k, f, j) {
        for (var h = 0; h < k.length; h++) {
            partId = "costumeSet_" + g + "_" + (j ? "s" : "") + k[h];
            $(partId).style.visibility = (f ? "visible" : "hidden")
        }
    };
    a(d, e, b, false);
    a(d, c, !b, true)
}
(function(a) {
    var b = {
        init: function() {
            setInterval(function() {
                a(".eyecatcher").addClass("eyecatcher-glow").delay(550).queue(function(c) {
                    a(this).removeClass("eyecatcher-glow");
                    c()
                })
            }, 3500);
            setInterval(function() {
                a(".eyecatcher-strong").addClass("eyecatcher-glow-strong").delay(1100).queue(function(c) {
                    a(this).removeClass("eyecatcher-glow-strong");
                    c()
                })
            }, 3500)
        }
    };
    a(function() {
        b.init();
        a(".meter > span").each(function() {
            a(this).data("origWidth", a(this).width()).width(0).animate({
                width: a(this).data("origWidth")
            }, 1200)
        });
        a(".new-meter").on("refresh", function() {
            var i = a(this)
              , j = i.data("value") || 0
              , f = i.data("max") || 100
              , g = i.data("caption");
            i.progressbar({
                value: j,
                max: f
            });
            var h = i.find(".label");
            if (g) {
                g = g.replace("#", j);
                if (!h.length) {
                    h = a('<div class="label">')
                }
                h.text(g).appendTo(i).css("top", (i.height() - h.height()) / 2)
            } else {
                h.remove()
            }
        }).trigger("refresh");
        a(".player-name").each(function() {
            a(this).autocomplete({
                source: "ajax.php?mod=player&submod=autoComplete&sh=" + secureHash,
                minLength: 2
            }).data("ui-autocomplete")._renderItem = function(g, h) {
                var f = a("<li>").text(h.label);
                if (h.value) {
                    f.wrapInner("<a>")
                } else {
                    f.addClass("ui-state-disabled")
                }
                return f.appendTo(g)
            }
        });
        a(".ellipsis").on("mouseenter.ellipsis", function() {
            var f = a(this);
            if (this.offsetWidth < this.scrollWidth && !f.attr("title")) {
                f.css("cursor", "help");
                f.attr("title", f.text().trim())
            }
            f.off("mouseenter.ellipsis")
        });
        a("#moveStack").click(function() {
            if (a(this).prop("checked")) {
                localStorage.setItem("moveStack", a(this).val())
            } else {
                localStorage.removeItem("moveStack")
            }
        }).prop("checked", !!localStorage.getItem("moveStack"));
        var e = a("#server-time");
        if (e.length) {
            var d = e.data("startTime");
            new ContinuousTimer(function(g, f) {
                e.text((new Date(f)).toLocaleString())
            }
            ,1000,(new Date(d[0],d[1] - 1,d[2],d[3],d[4],d[5],d[6])).getTime())
        }
        var c = ["enable", "main.php"];
        if (parent.document.getElementById("chatBox")) {
            c = ["disable", "index.php?mod=overview"];
            if (Browser.ie) {
                window.onmousewheel = document.onmousewheel = parent.wheel
            }
        }
        a("#chat_icon").addClass("chat_icon_" + c[0]).attr("href", c[1])
    })
}
)(jQuery);
;window.notifications = {
    popups: [],
    running: false,
    connect: function(a, b, c) {
        if (typeof EventSource === "function") {
            this.url = "https://" + document.domain + ":" + a + "/eventStream?type=ingame&playerId=" + b + "&session=" + c;
            this.doConnect()
        }
    },
    doConnect: function() {
        var a = this;
        this.source = new EventSource(this.url);
        this.source.addEventListener("headerUpdate", function(b) {
            headerObject.update(JSON.parse(b.data))
        });
        this.source.addEventListener("popup", function(b) {
            a.popups.push(JSON.parse(b.data));
            if (!a.running) {
                var c = function() {
                    if (!blackoutScreen) {
                        (new BlackoutDialog("notification")).set(a.popups.shift())
                    }
                    a.running = a.popups.length > 0;
                    if (a.running) {
                        setTimeout(c, 100)
                    }
                };
                c()
            }
        });
        this.source.addEventListener("error", function(b) {
            if (b.target.readyState !== EventSource.OPEN) {
                b.target.close();
                window.setTimeout(function() {
                    a.doConnect()
                }, 60000)
            }
        })
    }
};
;function selectDoll(a) {
    document.location.href = a
}
function selectTask(a, b) {
    sendRequest("get", "ajax/selectTask.php", "doll=" + a + "&task=" + b)
}
;;function formatZahl(d, c, b) {
    if (!c) {
        c = 0
    }
    var g = "";
    var e = Math.pow(10, c);
    d = "" + parseInt(d * e + (0.5 * (d > 0 ? 1 : -1))) / e;
    var a = d.indexOf(".");
    if (b) {
        d += (a == -1 ? "." : "") + e.toString().substring(1)
    }
    a = d.indexOf(".");
    if (a == -1) {
        a = d.length
    } else {
        g = "," + d.substr(a + 1, c)
    }
    while (a > 0) {
        if (a - 3 > 0) {
            g = "." + d.substring(a - 3, a) + g
        } else {
            g = d.substring(0, a) + g
        }
        a -= 3
    }
    return g
}
function PlingItem() {
    this.interval = 0;
    this.count = 0;
    this.id = 0;
    this.add = function(g, f, c, e, i, d, h, b) {
        var a = new Element("div");
        a.id = "pling" + b;
        a.addClass("pling");
        a.set("html", i);
        a.setStyle("color", d);
        a.setStyle("left", g);
        a.setStyle("top", f);
        a.setStyle("width", c);
        if ($(h)) {
            $(h).appendChild(a)
        } else {
            document.body.appendChild(a)
        }
        var j = this;
        this.interval = window.setInterval(function() {
            j.tick()
        }, 50);
        this.count = e;
        this.id = b
    }
    ;
    this.remove = function() {
        var a = $("pling" + this.id);
        if (a) {
            a.parentNode.removeChild(a)
        }
        window.clearInterval(this.interval);
        pling.remove(this.id)
    }
    ;
    this.tick = function() {
        var b = $("pling" + this.id);
        if (b) {
            var a = b.getStyle("top").toInt();
            a -= 2;
            this.count = Math.max(this.count - 2, 0);
            b.setStyle("top", a);
            if (this.count <= 0) {
                this.remove()
            }
        } else {
            this.remove()
        }
    }
}
function Pling() {
    this.aktPling = 1;
    this.items = [];
    this.items[0] = {};
    this.add = function(a, h, d, f, g, b, c) {
        var e = new PlingItem();
        e.add(a, h, d, f, g, b, c, this.aktPling);
        this.items[0][this.aktPling] = e;
        this.aktPling++
    }
    ;
    this.remove = function(a) {
        delete this.items[0][a]
    }
    ;
    this.currency = function(b, d, a, g) {
        var c = "";
        var e = "";
        var f = "";
        if (b != 0 || d != 0) {
            if (b > 0) {
                c = "#00FF21";
                e = "+"
            } else {
                c = "#FF0000";
                e = "-";
                b = -b;
                d = -d
            }
            f += '<div class="moneymessage">';
            if (b > 0) {
                f += '<span class="goldRow">' + e + " " + formatZahl(b) + "&nbsp;</span>"
            }
            if (d > 0) {
                f += '<br /><span class="rubyRow">' + e + " " + formatZahl(d) + "&nbsp;</span>"
            }
            f += "</div>";
            this.add(a, g, 150, 50, f, c, null)
        }
    }
    ;
    this.portrait = function(b, a, c) {
        b = '<span style="font-size:' + c + ';">' + b + "</span>";
        this.add(5, 80, 160, 50, b, a, "avatar")
    }
    ;
    this.item = function(a, d, c, b) {
        c = '<div class="itemmessage">' + c + "</div>";
        this.add(a, d, 190, 50, c, b, null)
    }
}
var pling = new Pling();
;function ProgressBar(b, d, e, f, a, h, g, c) {
    this.divIdText = b;
    this.divId = d;
    this.classProgress = e;
    this.classReady = f;
    this.readyText = a;
    this.start = h;
    this.nowInit = g;
    this.end = c;
    this.init = function() {
        this.now = this.nowInit;
        var i = this;
        jQuery(function() {
            i.update(true)
        })
    }
    ;
    this.update = function(k) {
        var j = this
          , o = 1000 * (this.end - this.now)
          , l = jQuery("#" + this.divId)
          , n = jQuery("#" + this.divIdText)
          , i = k ? 0 : 600;
        l.stop(true).removeClass(this.classReady);
        function m() {
            l.removeClass(j.classProgress).addClass(j.classReady);
            n.removeClass("ticker").text(j.readyText)
        }
        if (o <= 0) {
            l.css("width", "100%");
            m();
            return
        }
        l.addClass(this.classProgress).animate({
            width: Math.min(100 * (this.now - this.start) / (this.end - this.start), 100) + "%"
        }, i).animate({
            width: "100%"
        }, o - i, "linear", m);
        n.addClass("ticker").data({
            tickerTimeLeft: o + 1,
            tickerCallback: function() {
                j.now = j.end - (n.data("tickerTimeLeft") / 1000).round() - 1
            }
        });
        tickers.refresh(n.parent())
    }
}
;;(function(g) {
    var d, h, c;
    window.setPageKey = function(i) {
        d = i
    }
    ;
    function b(i) {
        return i.attr("id") || h.index(i)
    }
    function a(k) {
        var j = b(k);
        if (d in c && j in c[d]) {
            return !c[d][j]
        }
        var i = k.data("defaultState");
        if (g.type(i) === "boolean") {
            return i
        }
        return true
    }
    function e(k) {
        var i = b(k);
        if (g.type(i) === "string") {
            k = h.filter("#" + i)
        } else {
            k = g(h[i])
        }
        var j = a(k);
        k.toggle(j);
        if (j) {
            k.find(".new-meter").trigger("refresh")
        }
    }
    function f(i) {
        c[d][b(i)] = a(i);
        localStorage.setItem("collapsedSections", JSON.stringify(c));
        e(i)
    }
    g(function() {
        h = g(".section-header").find("+section");
        c = {};
        if (!h.length) {
            return
        }
        var l = {}
          , j = window.location.search.substr(1).split("&");
        if (!d) {
            for (var k = 0; k < j.length; k++) {
                var m = j[k].split("=", 2);
                l[m[0]] = decodeURIComponent(m[1] || "")
            }
            d = l.mod + "." + (l.submod || "index")
        }
        c = JSON.parse(localStorage.getItem("collapsedSections") || "{}");
        if (!c[d]) {
            c[d] = {}
        }
        h.prev().css("cursor", "pointer").mousedown(function(i) {
            i.preventDefault()
        }).click(function() {
            var i = g(this).find("+section");
            if (i.length) {
                localStorage.setItem("collapsedSections", JSON.stringify(c));
                f(i)
            }
        }).find("a[href]").click(function() {
            document.location.href = g(this).attr("href");
            return false
        });
        h.each(function() {
            var i = g(this);
            e(i)
        }).on("show", function() {
            if (!a(g(this))) {
                f(g(this))
            }
        })
    })
}
)(jQuery);
;jQuery(function() {
    var b = jQuery;
    var g = document.title;
    var d = b("body");
    var e = {}
      , i = 0;
    var j = /data-ticker-time-left=\\"(\d+)\\"/g;
    function c(l, n) {
        var k = Infinity;
        var m = !l;
        if (m) {
            l = d
        }
        if (!n) {
            n = 0
        }
        l.find(".ticker").each(function() {
            var v = b(this)
              , x = v.data("tickerTimeLeft")
              , u = v.data("tickerType")
              , r = (typeof x !== "number") ? -1 : parseInt(x) - n
              , w = v.data("tickerCallback")
              , q = (v.data("tickerReadyText") || "---");
            if (r < 0) {
                if (x > 0 && v.data("tickerRef")) {
                    var p = v.data("tickerLoc");
                    if (!p) {
                        p = document.location
                    }
                    document.location = p
                }
                v.removeClass("ticker")
            } else {
                k = Math.min(r, k);
                var o = v.hasClass("short");
                q = (v.data("tickerText") || "");
                if ("" !== q) {
                    q += " "
                }
                q = ("date" === u) ? a(q, r) : h(q, r, o)
            }
            v.data("tickerTimeLeft", r).html(q);
            if (w) {
                w(n)
            }
        });
        b.each(e, function() {
            var o = this.elem.data(this.key);
            if (this.obj) {
                o = JSON.stringify(o)
            }
            o = o.replace(j, function(p, q) {
                return 'data-ticker-time-left=\\"' + (q - n) + '\\"'
            });
            if (this.obj) {
                o = JSON.parse(o)
            }
            this.elem.data(this.key, o)
        });
        if (m) {
            if (k < Infinity) {
                document.title = h("", k) + " " + g
            } else {
                document.title = g
            }
        }
    }
    function f(m, l) {
        var n = b(m).data(l);
        if (!n) {
            return
        }
        var o = (typeof n == "object");
        if (o) {
            n = JSON.stringify(n)
        }
        var p = n.search(j);
        if (p > -1) {
            var k = i++;
            e[k] = {
                elem: b(m),
                key: l,
                obj: o
            };
            b(m).on("remove", function() {
                delete (e[k])
            })
        }
    }
    function a(l, m) {
        var k = new Date(Date.now() + m);
        var n = l.split("|");
        if (m > 1000 * 3600 * 24) {
            return n[0] + k.toLocaleDateString()
        } else {
            return n[1] + k.toLocaleTimeString()
        }
    }
    function h(n, p, l) {
        p = Math.round(p / 1000);
        var k = Math.floor(p / 60);
        var o = Math.floor(k / 60);
        var q = Math.floor(o / 24);
        o %= 24;
        k %= 60;
        p %= 60;
        if (p < 10) {
            p = "0" + p
        }
        if (k < 10) {
            k = "0" + k
        }
        if (!l) {
            var r = "d";
            n += ((q > 0) ? q + r + " " : "") + o + ":" + k + ":" + p
        } else {
            if (q > 0) {
                n += q + " d"
            } else {
                if (o > 0) {
                    n += o + " h"
                } else {
                    if (k > 0) {
                        n += k + " m"
                    } else {
                        n += p + " s"
                    }
                }
            }
        }
        return n
    }
    new ContinuousTimer(function(k) {
        c(null, k)
    }
    );
    window.tickers = {
        refresh: c,
        registerDataRefresh: f
    }
});
;jQuery(function() {
    var f = jQuery;
    var s = false;
    var k = 0;
    var o = typeof tickers !== "undefined";
    f(document).on("mouseenter.tooltip", "[data-tooltip]", function() {
        var t = f(this)
          , u = t.data("tooltip");
        if (s || !u) {
            return
        }
        u = u.slice();
        if (!t.is("#char div")) {
            l(t, u)
        }
        e(t, u)
    });
    if (o) {
        f("[data-tooltip]").each(function() {
            tickers.registerDataRefresh(f(this), "tooltip")
        })
    }
    f(document).mousedown(function() {
        s = true
    }).mouseup(function() {
        s = false
    }).on("dragend", function() {
        s = false
    });
    function q(t, u) {
        t = f(t);
        if ("string" == typeof u) {
            u = JSON.parse(u)
        }
        t.data("tooltip", u);
        h(t)
    }
    function r(u, v, t) {
        q(u, [[[v, t || "white"]]])
    }
    function b(u, t) {
        r(u, f(u).attr("title"), t);
        f(u).attr("title", "")
    }
    function h(t) {
        t = f(t);
        if (!t.attr("data-tooltip")) {
            t.attr("data-tooltip", "")
        }
        if (o) {
            tickers.registerDataRefresh(t, "tooltip")
        }
    }
    function l(w, y) {
        var u = w.data("contentType");
        var t = ".ui-droppable[data-content-type-accept=" + u + "]";
        var v = f("div", t);
        if (!v.length) {
            return
        }
        var x = w.data("comparisonTooltip");
        if (x instanceof Array) {
            Array.prototype.push.apply(y, x)
        } else {
            v.each(function() {
                if (0 == f(this).data("amount")) {
                    return
                }
                var z = f(this).data("tooltip");
                if (z instanceof Array) {
                    Array.prototype.push.apply(y, z)
                }
            })
        }
    }
    function e(x, y) {
        if (!y) {
            return
        }
        var v = p(y);
        var u = "mouseleave.tooltip dragstart.tooltip click.tooltip";
        var t = "closeTooltips." + k++;
        var w = function() {
            x.off(u + " mousemove.tooltip");
            f(document).off(t);
            v.remove()
        };
        f(document).trigger("closeTooltips").on(t, w);
        x.on("mousemove.tooltip", v, j).on(u, w);
        v.appendTo("body");
        if (o) {
            tickers.refresh(v)
        }
    }
    function p(w) {
        if ("string" == typeof w) {
            return f(w).css({
                position: "absolute",
                "z-index": 1000
            })
        }
        var t = w.length;
        for (var v = 0; v < t; v++) {
            w[v] = n(w[v])
        }
        var u = f('<section class="tooltips">');
        u.append(w);
        return u
    }
    function n(x) {
        var t = x.length;
        var w = f("<div>");
        for (var v = 0; v < t; v++) {
            var y = x[v];
            var u = [];
            if (y[0]instanceof Array && y[1]instanceof Array) {
                u = m(y)
            } else {
                if (y[0] != "") {
                    u = c(y)
                }
            }
            w.append(u)
        }
        return w
    }
    function m(z) {
        var y = z[0];
        var w = z[1];
        var u = f("<p>");
        var v = y.length;
        for (var x = 0; x < v; x++) {
            var t = f("<span>");
            t.attr("style", "color:" + w[x]).html(y[x]);
            u.append(t)
        }
        return u
    }
    function c(x) {
        var w = x[0];
        var u = x[1];
        var v = x[2] || 0;
        var t = f("<p>");
        t.attr("style", "color:" + u).html(w);
        if (v > 0) {
            t.css("max-width", v + "px")
        }
        return t
    }
    function a(D, G) {
        var H = D.data("current");
        var O = null == H;
        var t = D.children()
          , J = t.length - 1;
        var F = Math.max(0, Math.min(J, (H || 0) + (G || 0)));
        var v = f(t[F])
          , C = v.width()
          , K = v.height();
        var E = window.innerWidth
          , L = window.innerHeight;
        var N = f(window)
          , M = N.scrollLeft()
          , I = N.scrollTop();
        var A = Math.max(M, Math.min(D.data("ox") - C / 2, M + E - C));
        var z = Math.max(I, Math.min(D.data("oy") - K / 2, I + L - K));
        var P = (f("body").css("direction") == "rtl") ? -1 : 1;
        var B = {
            left: A,
            top: z
        };
        if (F != H) {
            B.width = C + 6;
            B.height = K + 6;
            if (!O) {
                var u = -(C + 6);
                if (G > 0) {
                    u = f(t[H]).width() + 6
                }
                B.scrollLeft = "+=" + (P * u)
            }
        }
        if (O) {
            D.css(B)
        } else {
            D.animate(B)
        }
        D.data("current", F)
    }
    function g(t) {
        t.addClass("mobile");
        t.children().each(function() {
            f(this).css({
                width: (f(this).width() + 1) + "px",
                height: (f(this).height() + 1) + "px"
            })
        });
        t.find(">div:not(:first-child)").prepend(function() {
            return f("<div>").addClass("tooltip-prev").on("touchend", function(u) {
                a(t, -1);
                u.stopPropagation();
                u.preventDefault()
            })
        });
        t.find(">div:not(:last-child)").append(function() {
            return f("<div>").addClass("tooltip-next").on("touchend", function(u) {
                a(t, 1);
                u.stopPropagation();
                u.preventDefault()
            })
        });
        a(t, 0)
    }
    function d(u) {
        var t = 0;
        u.children().each(function() {
            t += f(this).outerWidth(true)
        });
        u.children().each(function() {
            var w = f(this).outerWidth(true)
              , v = Math.floor(f(this).width() - 8);
            f(this).children().each(function() {
                if (f(this).width() < v) {
                    f(this).css("max-width", v + "px")
                }
            });
            var x = 100 * w / t;
            f(this).css("max-width", x + "%");
            if (w > 200) {
                f(this).css("min-width", "200px")
            }
        });
        t += u.children().length;
        u.css("max-width", t + "px")
    }
    function j(A, E, D, G) {
        var z = A.data;
        if (G) {
            z.data({
                ox: E,
                oy: D
            });
            if (!z.is(".mobile")) {
                g(z)
            }
        } else {
            var B = f(window);
            var t = B.width()
              , C = B.height();
            var u = B.scrollLeft()
              , H = B.scrollTop();
            if ("none" == z.children().first().css("max-width")) {
                d(z)
            }
            var F = z.width()
              , v = z.height();
            E = i(z, "x", E || A.pageX, F, u + t);
            D = i(z, "y", D || A.pageY, v, H + C);
            E = Math.max(u, Math.min(E, u + t - F));
            D = Math.max(32, H, Math.min(D, H + C - v));
            z.css("pointer-events", "none");
            z.css({
                left: E,
                top: D
            })
        }
    }
    function i(v, x, A, u, t) {
        var z = "align" + x.toUpperCase();
        if (!v.data(z)) {
            v.data(z, (A <= t / 2 || t - A > u) ? 1 : -1);
            if ("x" == x && f("body").css("direction") == "rtl") {
                if (A - f(window).scrollLeft() > u) {
                    v.data(z, -1)
                }
            }
        }
        var y = v.data(z);
        if ("y" === x) {
            var w = v.children();
            u = (v.data("alignX") < 0 ? w.last() : w.first()).height()
        }
        return (y < 0) ? A - u - 10 : A + 10
    }
    window.tooltips = {
        set: q,
        setSimple: r,
        setFromTitle: b,
        init: h
    }
});
;jQuery(function() {
    var c = jQuery;
    var b = false;
    c(document).on("dragstart", function() {
        b = true
    }).on("dragend", function() {
        b = false
    });
    function d(j, f) {
        if (1 < j.originalEvent.touches.length) {
            return
        }
        var l = j.originalEvent.changedTouches[0]
          , i = j.type.substr(5);
        var g = "mouse" + {
            start: "down",
            move: "move",
            end: "up"
        }[i];
        var k = c.extend({
            bubbles: true,
            cancelable: true,
            view: window
        }, l);
        var h = new MouseEvent(g,k);
        l.target.dispatchEvent(h);
        if (c(l.target).is(".ui-draggable") || b || f) {
            j.stopImmediatePropagation();
            j.stopPropagation();
            j.preventDefault()
        }
        return h
    }
    var e, a = false;
    c(document).on("touchstart", function(g) {
        d(g);
        var h = c(g.target);
        e = h;
        while (!h.is("body")) {
            if (h.is(".ui-draggable")) {
                break
            }
            if (h.is("a, input, textarea, select, label") || h.attr("onClick")) {
                e = null;
                break
            }
            var f = c._data(h.get(0), "events");
            if (f && f.click) {
                e = null;
                break
            }
            h = h.parent()
        }
        a = false
    });
    c(document).on("touchmove", function(f) {
        d(f);
        if (e && e.is(".ui-draggable")) {
            c(document).trigger("closeTooltips")
        }
        a = true
    });
    c(document).on("touchend", function(f) {
        var g = e && !a;
        f = d(f, g);
        if (g) {
            c(document).trigger("closeTooltips");
            e.trigger("mouseenter.tooltip", [true]);
            e.trigger("mousemove.tooltip", [f.pageX, f.pageY, true])
        }
        e = false
    });
    c.each(c.ui.draggable.prototype.plugins.drag, function(f, g) {
        if ("scroll" == g[0]) {
            g[1] = function(l, m, k) {
                var n = k.options
                  , j = false
                  , p = k.scrollParentNotHidden[0]
                  , h = k.document[0];
                if (p !== h && p.tagName !== "HTML") {
                    if (!n.axis || n.axis !== "x") {
                        if ((k.overflowOffset.top + p.offsetHeight) - l.pageY < n.scrollSensitivity) {
                            p.scrollTop = j = p.scrollTop + n.scrollSpeed
                        } else {
                            if (l.pageY - k.overflowOffset.top < n.scrollSensitivity) {
                                p.scrollTop = j = p.scrollTop - n.scrollSpeed
                            }
                        }
                    }
                    if (!n.axis || n.axis !== "y") {
                        if ((k.overflowOffset.left + p.offsetWidth) - l.pageX < n.scrollSensitivity) {
                            p.scrollLeft = j = p.scrollLeft + n.scrollSpeed
                        } else {
                            if (l.pageX - k.overflowOffset.left < n.scrollSensitivity) {
                                p.scrollLeft = j = p.scrollLeft - n.scrollSpeed
                            }
                        }
                    }
                } else {
                    if (!n.axis || n.axis !== "x") {
                        if (l.pageY - c(h).scrollTop() < n.scrollSensitivity) {
                            j = c(h).scrollTop(c(h).scrollTop() - n.scrollSpeed)
                        } else {
                            if (window.innerHeight - (l.pageY - c(h).scrollTop()) < n.scrollSensitivity) {
                                j = c(h).scrollTop(c(h).scrollTop() + n.scrollSpeed)
                            }
                        }
                    }
                    if (!n.axis || n.axis !== "y") {
                        if (l.pageX - c(h).scrollLeft() < n.scrollSensitivity) {
                            j = c(h).scrollLeft(c(h).scrollLeft() - n.scrollSpeed)
                        } else {
                            if (window.innerWidth - (l.pageX - c(h).scrollLeft()) < n.scrollSensitivity) {
                                j = c(h).scrollLeft(c(h).scrollLeft() + n.scrollSpeed)
                            }
                        }
                    }
                }
                if (j !== false && c.ui.ddmanager && !n.dropBehaviour) {
                    c.ui.ddmanager.prepareOffsets(k, l)
                }
            }
        }
    })
});
;