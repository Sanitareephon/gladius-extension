// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

// sample code
// chrome.runtime.onInstalled.addListener(function () {
//   chrome.storage.sync.set({ color: '#3aa757' }, function () {
//     console.log("The color is green.");
//   });
// });

let tabIdList = [];

chrome.webNavigation.onCompleted.addListener(function (currentTab) {
  executeScript('script/gladiatusScript.js', currentTab);
  executeScriptAllMatch('script/editPage.js', currentTab);
}, { url: [{ urlMatches: 'https://s40-en.gladiatus.gameforge.com' }] });


function executeScript(fileScript, currentTab) {
  chrome.tabs.query({}, function (tabs) {
    let gladiatusTab = tabs.find(tab => tab.url.match("https://s40-en.gladiatus.gameforge.com/*"));

    // Execute Gladiatus First Tab
    if (gladiatusTab && gladiatusTab.id === currentTab.tabId) {
      setBotConfigLocalStorage(gladiatusTab);
      validateAutoBidSetting(gladiatusTab);
      chrome.tabs.executeScript(gladiatusTab.id, { file: fileScript });
      setActiveTabLocalStorage(gladiatusTab)
    }
  });
}

function executeScriptAllMatch(fileScript, currentTab) {
  chrome.tabs.query({}, function (tabs) {
    let gladiatusTabList = tabs.filter(tab => tab.url.match("https://s40-en.gladiatus.gameforge.com/*"));

    // Execute Gladiayus All Tab
    gladiatusTabList.forEach((tab, index) => {
      if (tab && tab.id === currentTab.tabId && index !== 0) {
        chrome.tabs.executeScript(tab.id, { file: fileScript });
      }
    });
  });
}

// Background function
function activeGladiatusTab(openTabConfig, gladiatusTab) {
  if (openTabConfig.isOpenTabFirstTime && openTabConfig.isOpenAuctionHouse) {
    chrome.tabs.executeScript(gladiatusTab.id, { code: "localStorage.setItem('openTabConfig', JSON.stringify({isOpenTabFirstTime: false, isOpenAuctionHouse: " + openTabConfig.isOpenAuctionHouse + "}));" });
    chrome.tabs.update(gladiatusTab.id, { active: true });
  }
}

function validateAutoBidSetting(gladiatusTab) {
  chrome.tabs.executeScript(gladiatusTab.id, { code: "var x = JSON.parse(localStorage.getItem('autoBidSetting')); x" }, (autoBidSetting) => {
    if (autoBidSetting[0]?.isContinueBidding === false) {
      let removeLocalStorage = "localStorage.removeItem('autoBidSetting');"
      chrome.tabs.executeScript(gladiatusTab.id, { code: removeLocalStorage }, (autoBidSetting) => { });
      chrome.storage.sync.get('botConfig', function (results) {
        if (Object.keys(results).length > 0) {
          let botConfig = JSON.parse(results.botConfig);
          botConfig.bidItem = '';
          botConfig.bidItemLevel = '';
          botConfig.autoBid = false;
          chrome.storage.sync.set({ botConfig: JSON.stringify(botConfig) }, function () { });
        }
      });
    }
  });
}

function setActiveTabLocalStorage(gladiatusTab) {
  chrome.tabs.executeScript(gladiatusTab.id, { code: "var x = JSON.parse(localStorage.getItem('openTabConfig')); x" }, (openTabConfig) => {
    activeGladiatusTab(openTabConfig[0], gladiatusTab)
  });
}

function setBotConfigLocalStorage(gladiatusTab) {
  chrome.storage.sync.get('botConfig', function (results) {
    chrome.tabs.executeScript(gladiatusTab.id, { code: "localStorage.setItem('botConfig',JSON.stringify(" + results.botConfig + "));" });
  });
}