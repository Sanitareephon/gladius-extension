// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
let elementConfig = [
  { elementId: 'autoBid', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoCheckTimeGladiatorAuction', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoCheckTimeMerceneryAuction', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoDungeon', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoExpedition', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoRevengeOfTheDead', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoSelectPantheon', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoTraining', elementType: 'checkbox', defaultValue: false },
  { elementId: 'autoWorking', elementType: 'text', defaultValue: 2 },
  { elementId: 'bidAfterTime', elementType: 'text', defaultValue: '90' },
  { elementId: 'bidItem', elementType: 'text', defaultValue: '' },
  { elementId: 'bidItemLevel', elementType: 'text', defaultValue: '' },
  { elementId: 'fightMonster', elementType: 'text', defaultValue: '' },
  { elementId: 'loopTime', elementType: 'text', defaultValue: '5' },
  { elementId: 'minimumHpFighting', elementType: 'text', defaultValue: '30' },
  { elementId: 'revengeMonster', elementType: 'text', defaultValue: '' },
  { elementId: 'soundGladiatorAuction', elementType: 'text', defaultValue: 'https://iringtone.net/uploads/audio/2018-02/11878/rington.mp3' },
  { elementId: 'soundMerceneryAuction', elementType: 'text', defaultValue: 'https://iringtone.net/uploads/audio/2019-02/18109/rington.mp3' },
  { elementId: 'timeGladiatorAuctionState', elementType: 'text', defaultValue: 'Very Short' },
  { elementId: 'timeMerceneryAuctionState', elementType: 'text', defaultValue: 'Very Short' },
];

chrome.storage.sync.get('botConfig', function (results) {
  if (Object.keys(results).length > 0) {
    botConfig = JSON.parse(results.botConfig);
  } else {
    setDefaultBotConfig();
  }
  constructOptions();
  onAutoBidChecking();
});

function constructOptions() {
  elementConfig.forEach((config) => {
    let element = document.getElementById(config.elementId);
    switch (config.elementType) {
      case 'checkbox':
        element.addEventListener('change', () => { updateBotConfig(); });
        element.checked = botConfig[config.elementId] ? botConfig[config.elementId] : config.defaultValue;
        break;
      case 'text':
        element.addEventListener('input', () => { updateBotConfig(); });
        element.value = botConfig[config.elementId] ? botConfig[config.elementId] : config.defaultValue;
        break;
    }
  })
}

function onAutoBidChecking() {
  document.getElementById('autoBid').addEventListener('change', () => {
    if (document.getElementById('autoBid').checked) {
      document.getElementById('autoCheckTimeGladiatorAuction').checked = true;
      updateBotConfig();
    }
  });

  document.getElementById('autoCheckTimeGladiatorAuction').addEventListener('change', () => {
    if (!document.getElementById('autoCheckTimeGladiatorAuction').checked) {
      document.getElementById('autoBid').checked = false;
      updateBotConfig();
    }
  });

  document.getElementById('autoCheckTimeGladiatorAuction').addEventListener('change', () => {
      document.getElementById('autoCheckTimeMerceneryAuction').checked = document.getElementById('autoCheckTimeGladiatorAuction').checked;
      updateBotConfig();
  });

  document.getElementById('autoCheckTimeMerceneryAuction').addEventListener('change', () => {
      document.getElementById('autoCheckTimeGladiatorAuction').checked = document.getElementById('autoCheckTimeMerceneryAuction').checked;
      updateBotConfig();
  });
}

function setDefaultBotConfig() {
  elementConfig.forEach((config) => {
    botConfig[config.elementId] = config.defaultValue;
  });
  chrome.storage.sync.set({ botConfig: JSON.stringify(botConfig) }, function () { });
}

function updateBotConfig() {
  elementConfig.forEach((config) => {
    let element = document.getElementById(config.elementId);
    switch (config.elementType) {
      case 'checkbox': botConfig[config.elementId] = element.checked; break;
      case 'text': botConfig[config.elementId] = element.value; break;
    }
  });
  chrome.storage.sync.set({ botConfig: JSON.stringify(botConfig) }, function () { });
}
