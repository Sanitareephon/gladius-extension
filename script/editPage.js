let secureHash = '';
getSecureHash();
createButtons();

// Page Editor
function createButtons() {
    // Condition
    let isCreateMoveAllResource = !!xpath("//a[contains(text(),'Packages')]");
    let moveResourceParentElement = xpath("//form[@name='filterForm']//fieldset");

    let isCreateSellAll = !!xpath("//div[contains(@class,'shopTab dynamic')]");
    let sellAllParentElement = xpath("//div[@id='content']//form//span");

    let isCreateRemoveMessage = !!xpath("//a[@class='awesome-tabs current'][contains(text(),'Folders')]");
    let removeMessageParentElement = xpath("//div[@class='paging gca_paging']");

    createButton('Move Resources', moveResourceParentElement, moveResources, isCreateMoveAllResource);
    createButton('Sell All', sellAllParentElement, sellAllItem, isCreateSellAll);
    createButton('Remove Messge', removeMessageParentElement, removeMessage, isCreateRemoveMessage);
}

// Button Function
function moveResources() {
    let defaultMoveResourceLink = window.location.origin + '/game/ajax.php?mod=inventory&submod=move&';
    let inventoryToXY = inventoryDropablePosition('to');
    let to = xpath("//a[@class='awesome-tabs current'][@data-bag-number]").getAttribute('data-bag-number');

    // Move Resources
    inventoryToXY.forEach((toXYQuery, index) => {
        let packageLength = document.querySelectorAll('.packageItem div[data-container-number]').length;
        if (index <= packageLength - 1) {
            // Condition
            let packageElement = document.querySelectorAll('.packageItem div[data-container-number]')[index];
            let from = packageElement.getAttribute('data-container-number');

            let fromX = packageElement.children[0].getAttribute('data-position-x');
            let fromY = packageElement.children[0].getAttribute('data-position-y');
            let amount = packageElement.children[0].getAttribute('data-amount');

            let fromQuery = 'from=' + from;
            let fromXQuery = 'fromX=' + fromX;
            let fromYQuery = 'fromY=' + fromY;

            // Query String, Body
            let queryString = fromQuery + '&' + fromXQuery + '&' + fromYQuery + '&' + toXYQuery + '&amount=' + amount;
            let moveResourceLink = defaultMoveResourceLink + queryString;
            let body = '&a=' + new Date().getTime() + '&sh=' + secureHash;

            setTimeout(() => {
                ajaxPost(moveResourceLink, body, (res) => { console.log(res.responseText); });
            }, 500);
        }
    });
}

function removeMessage() {
    document.querySelectorAll('div[class="message_text"]').forEach((data) => {
        if (data.textContent.includes('The following member left your guild:')) {
            data.parentElement.parentElement.querySelector('input[type="checkbox"]').click();
        }
    })
    document.querySelectorAll("div[class='message_title']").forEach((messageElement) => {
        let work = messageElement.textContent.includes('Message from work');
        let level = messageElement.textContent.includes('You`ve reached a new level');
        let sold = messageElement.textContent.includes('Item sold at the market');
        let auction = messageElement.textContent.includes('You have won an auction.');
        let outbid = messageElement.textContent.includes('You have been outbid at an auction.');
        if (work || level || sold || auction || outbid) {
            messageElement.parentElement.parentElement.querySelector("input[type='checkbox']").click();
        }
    });
    setTimeout(() => {
        xpath("//input[@value='Go!']").click();
    }, 500);
}

function sellAllItem() {
    let defaultMoveResourceLink = window.location.origin + '/game/ajax.php?mod=inventory&submod=move&';

    // Inventory Dragable Area
    let inventoryFromXY = inventoryDragablePosition('from');
    // Sell Dropable Area
    let shopToXY = shopDropablePosition('to');

    inventoryFromXY.forEach((fromXYText, index) => {
        if (index <= shopToXY.length - 1) {
            let doll = document.querySelector('#plDoll').value;

            // Query String, Body
            let queryString = fromXYText + '&' + shopToXY[index] + '&doll=' + doll;
            let moveResourceLink = defaultMoveResourceLink + queryString;
            let body = '&a=' + new Date().getTime() + '&sh=' + secureHash;

            setTimeout(() => {
                ajaxPost(moveResourceLink, body, (res) => { console.log(res.responseText); });
            }, 500);
        }
    });

}


// Library function
function ajaxCall(urlRequest, callbackFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            let responsePageElement = convertTextToHTMLElement('<div>' + xhr.responseText + '</div>');
            callbackFunction(responsePageElement);
        }
    };
    xhr.open('GET', urlRequest);
    xhr.send('&a=1606580967832&sh=440072d8d7494fa865a4c101fbc00831');
}

function ajaxPost(urlRequest, body, callbackFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            let responsePageElement = convertTextToHTMLElement(xhr.responseText);
            callbackFunction(responsePageElement);
        }
    };
    xhr.open('POST', urlRequest);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send(body);
}

function convertTextToHTMLElement(innerHTML) {
    let tempDiv = document.createElement('div'); tempDiv.innerHTML = innerHTML;
    return tempDiv.firstChild;
}

function getLocalStorage(key) {
    let value = localStorage.getItem(key)
    return JSON.parse(value ? value : null);
}

function removeLocalStorage(key) {
    localStorage.removeItem(key);
}

function setLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
}

function xpath(query, object) {
    var obj = object ?
        object.children[0]?.parentNode :
        document.children[0];
    return document.evaluate(query, obj, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

function createElement(innerHTML) {
    var tempDiv = document.createElement("div");
    tempDiv.innerHTML = innerHTML;
    return tempDiv.firstChild;
}

function createButton(buttonName, attachElement, callBack, isCreate) {
    if (isCreate) {
        let buttonId = camelCase(buttonName);
        let holderElement = createElement("<input id='" + buttonId + "' type='button' class='awesome-button' value='" + buttonName + "'></input>")
        attachElement?.appendChild(holderElement);
        xpath("//input[@id='" + buttonId + "']").onclick = callBack;
    }
}

function camelCase(str) {
    let word = str.split(' ');
    let camelCaseWord = '';
    for (let i = 0; i <= word.length - 1; i++) {
        let currentWord = word[i];
        if (i === 0) {
            camelCaseWord += currentWord.toLowerCase();
        } else {
            camelCaseWord += currentWord[0].toUpperCase() + currentWord.toLowerCase().substring(1, currentWord.length);
        }
    }
    return camelCaseWord;
}

function getSecureHash() {
    let scriptText = xpath("//script[@type='text/javascript'][contains(text(),'secureHash')]").textContent;
    secureHash = scriptText.match(/var secureHash    = "(\d+|\w+)";/)[0].replace('var secureHash    = "', "").replace('";', '');
}

function xpath(query, object) {
    var obj = object ?
        object.children[0]?.parentNode :
        document.children[0];
    return document.evaluate(query, obj, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

function inventoryDragablePosition(position) {
    let xy = [];
    let dataBagNumber = xpath("//a[@class='awesome-tabs current'][@data-bag-number]").getAttribute('data-bag-number');
    // Inventory Droppable Area
    document.querySelectorAll('#inv div[data-tooltip]').forEach((boxElement) => {
        let x = +boxElement.style.left.replace('px', '');
        let y = +boxElement.style.top.replace('px', '');
        x = x === 0 ? x + 1 : (x / 32) + 1;
        y = y === 0 ? y + 1 : (y / 32) + 1;

        let amountQuery = position === 'from' ? '&amount=' + boxElement.getAttribute('data-amount') : '';
        let dataBagQuery = position + '=' + dataBagNumber;
        let positionXQuery = position + 'X=' + x;
        let positionYQuery = position + 'Y=' + y;

        xy.push(dataBagQuery + '&' + positionXQuery + '&' + positionYQuery + amountQuery);
    });

    return xy;
}

function inventoryDropablePosition(position) {
    let xy = [];
    let bagNumber = xpath("//a[@class='awesome-tabs current'][@data-bag-number]").getAttribute('data-bag-number');
    // Inventory Dragable Area
    document.querySelectorAll('#inv .ui-droppable.grid-droparea').forEach((boxElement) => {
        let x = +boxElement.style.left.replace('px', '');
        let y = +boxElement.style.top.replace('px', '');
        x = x === 0 ? x + 1 : (x / 32) + 1;
        y = y === 0 ? y + 1 : (y / 32) + 1;

        let positionQuery = position === 'to' ? position + '=' + bagNumber + '&' : '';
        let positionXQuery = position + 'X=' + x;
        let positionYQuery = position + 'Y=' + y;

        xy.push(positionQuery + positionXQuery + '&' + positionYQuery);
    });

    return xy;
}

function shopDropablePosition(position) {
    let xy = [];
    let containerNumber = document.querySelector('div#shop[data-container-number]').getAttribute('data-container-number');
    // Shop Dropable Area
    document.querySelectorAll('#shop .ui-droppable.grid-droparea').forEach((boxElement) => {
        let x = +boxElement.style.left.replace('px', '');
        let y = +boxElement.style.top.replace('px', '');
        x = x === 0 ? x + 1 : (x / 32) + 1;
        y = y === 0 ? y + 1 : (y / 32) + 1;

        let positionQuery = position === 'to' ? position + '=' + containerNumber + '&' : '';
        let positionXQuery = position + 'X=' + x;
        let positionYQuery = position + 'Y=' + y;

        xy.push(positionQuery + positionXQuery + '&' + positionYQuery);
    });

    return xy;
}