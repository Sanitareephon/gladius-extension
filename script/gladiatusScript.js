let botConfig = null;
let recursiveTime = 0;
let maxRecursiveRefreshTime = 50;
let isContinueNextProcess = true;
let audioGladiator = null;
let audioMercenery = null;
let secureHash = '';
console.log("Script active");

getSecureHash();
setBotConfig();
createAudioSound();

let gladiatorNecessitiesAuctionHouseLink = xpath("//a[contains(text(), 'Auction house')]")?.href;
let merceneryNecessitiesAuctionHouseLink = window.location.origin + "/game/index.php?mod=auction&qry=&itemLevel=40&itemType=0&itemQuality=-1&ttype=3&sh=" + secureHash;

loopScript();

// Running funciton as loop
function loopScript() {
    clearNotification();
    console.log('loop recursive: ', recursiveTime);
    if (botConfig.autoCheckTimeGladiatorAuction) { checkRemainingTimeAuction(gladiatorNecessitiesAuctionHouseLink); }
    if (botConfig.autoCheckTimeMerceneryAuction) { checkRemainingTimeAuction(merceneryNecessitiesAuctionHouseLink); }
    if (botConfig.autoBid) { autoBidItem(); }
    let isContinueBidding = getLocalStorage('autoBidSetting')?.isContinueBidding;
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoSelectPantheon) { checkPantheonAvailable(); selectPantheon(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoExpedition && botConfig.fightMonster) { battleExpedition(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoDungeon) { battleDungeon(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoExpedition && botConfig.autoRevengeOfTheDead) { battleRevengeOfDead(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoExpedition && botConfig.autoRevengeOfTheDead) { checkBattleRevengeOfDead(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoTraining) { checkTrainingAvailable(); trainingGladiator(); }
    if (isContinueNextProcess && !isContinueBidding && botConfig.autoWorking > 0) { workingLabour(botConfig.autoWorking); }
    recursiveTime = recursiveTime + 1;
    if (recursiveTime >= maxRecursiveRefreshTime) {
        window.location.reload();
    }
    setTimeout(() => { loopScript() }, (botConfig.loopTime * 1000));
}

// Gladiatus page scipt
function autoBidItem() {
    let remainingTime = xpath("//span[@class='description_span_right']")?.textContent;
    let autoBidSetting = getLocalStorage('autoBidSetting');
    if (remainingTime) {
        let isStartAutoBid = remainingTime.toLowerCase() === expectMarketDuration.toLowerCase();
        if (isStartAutoBid || autoBidSetting?.isContinueBidding === true) {
            if (autoBidSetting) {
                let isTimeToBid = new Date().getTime() >= +autoBidSetting.bidTime;
                if (isTimeToBid) {
                    let itemTypeBidding = xpath("//select[@name='itemType']");
                    if (itemTypeBidding.value === '0') {
                        let bidElementXPath = "//div[@class='auction_item_div']//div[contains(@data-tooltip, '" + botConfig.bidItem + "')][@data-level='" + botConfig.bidItemLevel + "']";
                        let isBidSuccess = xpath(bidElementXPath + "/../../..//a/span");
                        let bidItemElement = xpath(bidElementXPath);
                        if (bidItemElement && !isBidSuccess) {
                            let bidAmountElement = xpath(bidElementXPath + "/../../..//input[@name='bid_amount']");
                            let bidButtonElement = xpath(bidElementXPath + "/../../..//input[@name='bid']");
                            bidAmountElement.value = +bidAmountElement.value;
                            bidButtonElement.click();
                        } else if (!bidItemElement) {
                            // playAudio();
                            autoBidSetting.isContinueBidding = false;
                            setLocalStorage('autoBidSetting', autoBidSetting);
                        }
                    } else {
                        itemTypeBidding.value = '0';
                        let filterButton = xpath("//input[@value='Filter']");
                        setTimeout(() => { filterButton.click(); }, 5);
                    }
                }
            } else {
                let startTimer = new Date().getTime();
                let bidTime = new Date(new Date().getTime() + (+botConfig.bidAfterTime * 1000)).getTime();
                setLocalStorage('autoBidSetting', { startTimer: startTimer, bidTime: bidTime, isContinueBidding: true });
            }
        }
    } else if (!remainingTime && autoBidSetting) {
        window.location.href = xpath("//a[contains(text(), 'Auction house')]")?.href;
    }
}

function battleExpedition() {
    let battleExpeditionElement = document.querySelector('#cooldown_bar_expedition #cooldown_bar_text_expedition');
    let isExpeditionAvailable = battleExpeditionElement.textContent === "Go to expedition";
    let isInExpeditionPage = xpath("//div[@class='expedition_name'][contains(text(), '" + botConfig.fightMonster + "')]");
    let currentHp = getCurrentHp();
    if (isExpeditionAvailable && currentHp > botConfig.minimumHpFighting) {
        let battleExpeditionListElement = document.querySelector('#expedition_list')?.parentElement;
        if (!isInExpeditionPage) {
            battleExpeditionElement.parentElement.querySelector('a').click();
        } else if (isInExpeditionPage && battleExpeditionListElement) {
            fightingWith(botConfig.fightMonster);
        }
    }
}

function battleDungeon() {
    let battleDungeonElement = document.querySelector('.cooldown_bar_text#cooldown_bar_text_dungeon');
    let isDungeonAvailable = battleDungeonElement.textContent === "Go to dungeon";
    let currentHp = getCurrentHp();
    if (isDungeonAvailable && currentHp > botConfig.minimumHpFighting) {
        let enterDungeonButton = xpath("//h3[contains(text(), 'Enter Dungeon')]/..//input[@value='Normal']");
        let fightDungeonButton = xpath("//img[@src='9379/img/combatloc.gif'][last()]");
        if (enterDungeonButton) {
            enterDungeonButton.click();
        } else if (fightDungeonButton) {
            fightDungeonButton.click();
        } else if (!enterDungeonButton && !fightDungeonButton) {
            battleDungeonElement.parentElement.querySelector('a').click();
        }
    }
}

function battleRevengeOfDead() {
    let isInRevengePage = xpath("//p[contains(text(), 'You are currently in the event area \"Revenge of the Dead\".')]");
    let battleExpeditionElement = document.querySelector('#expedition_list');
    let isZeroPoint = xpath("//p[contains(text(), 'Your free event points: 0')]");
    let currentHp = getCurrentHp();
    if (isInRevengePage && !isZeroPoint && battleExpeditionElement && currentHp > botConfig.minimumHpFighting) {
        fightingWith(botConfig.revengeMonster);
    }
}

function checkBattleRevengeOfDead() {
    let revengeOfDeadLink = xpath("//a[contains(text(), 'Revenge of the Dead')]").href;
    ajaxCall(revengeOfDeadLink, checkBattleRevengeOfDeadCallBack);
}

function checkBattleRevengeOfDeadCallBack(resPageElement) {
    let isTimeRemaining = xpath("//span[@data-ticker-text='Time remaining until your next expedition: ']", resPageElement);
    let isZeroPoint = xpath("//p[contains(text(), 'Your free event points: 0')]", resPageElement);
    if (!isTimeRemaining && !isZeroPoint) {
        xpath("//a[contains(text(), 'Revenge of the Dead')]").click();
    }
}

function checkPantheonAvailable() {
    let pantheonLink = xpath("//a[contains(text(), 'Pantheon')]").href;
    ajaxCall(pantheonLink, checkPantheonAvailableCallBack);
}

function checkPantheonAvailableCallBack(resPageElement) {
    let questHeaderAccepted = xpath("//div[@id='quest_header_accepted']", resPageElement)?.textContent;
    let pantheonCooldown = xpath("//div[@id='quest_header_cooldown'][contains(text(), 'Remaining time until you can accept a new quest:')]", resPageElement);
    let pantheonComplete = xpath("//a[@class='quest_slot_button quest_slot_button_finish']", resPageElement);
    if ((!pantheonCooldown && questHeaderAccepted !== 'Accepted quests: 5 / 5') || pantheonComplete) {
        xpath("//a[contains(text(), 'Pantheon')]").click();
    }
}

function checkRemainingTimeAuction(auctionHouseLink) {
    ajaxCall(auctionHouseLink, checkRemainingTimeAuctionCallBack);
}

function checkRemainingTimeAuctionCallBack(resPageElement) {
    let currentRemainingTime = xpath("//span[@class='description_span_right']")?.textContent.toLowerCase() || '';
    let remainingTime = xpath("//span[@class='description_span_right']", resPageElement)?.textContent.toLowerCase() || '';
    let isGladiatorCallback = xpath("//a[@class='awesome-tabs current'][contains(text(), 'Gladiator necessities')]", resPageElement);
    let isMerceneryCallback = xpath("//a[@class='awesome-tabs current'][contains(text(), 'Mercenary necessities')]", resPageElement);
    let expectMarketDuration = isGladiatorCallback ? botConfig.timeGladiatorAuctionState.toLowerCase() : botConfig.timeMerceneryAuctionState.toLowerCase();
    let isOpenAuctionHouse = remainingTime === expectMarketDuration;
    let isFoundGladiatorAuction = currentRemainingTime === botConfig.timeGladiatorAuctionState.toLowerCase();
    let isFoundMerceneryAuciton = currentRemainingTime === botConfig.timeMerceneryAuctionState.toLowerCase();
    let isFoundAuctionTime = isFoundGladiatorAuction || isFoundMerceneryAuciton;

    if (isOpenAuctionHouse) {
        let isOpenTabFirstTime = getLocalStorage('openTabConfig') ? getLocalStorage('openTabConfig').isOpenTabFirstTime : true;
        let openTabConfig = { 'isOpenTabFirstTime': isOpenTabFirstTime, isOpenAuctionHouse: isOpenAuctionHouse };
        setLocalStorage('openTabConfig', openTabConfig);

        // Logging Auction House
        let auctionHouseLogging = getLocalStorage('auctionHouseShortTimeLogging');
        auctionHouseLogging = auctionHouseLogging ? auctionHouseLogging : [];
        auctionHouseLogging.push({ remainingTime: remainingTime, timestamp: new Date() });
        setLocalStorage('auctionHouseShortTimeLogging', auctionHouseLogging);

        // Open Auction House Once Per Round
        if (remainingTime !== currentRemainingTime) {
            isContinueNextProcess = false;
            let auctionHouseLink = isGladiatorCallback ? gladiatorNecessitiesAuctionHouseLink : merceneryNecessitiesAuctionHouseLink;
            window.location.href = auctionHouseLink;
        } else {
            let isGladiator = xpath("//a[@class='awesome-tabs current'][contains(text(), 'Gladiator necessities')]");
            let audio = !!isGladiator ? audioGladiator : audioMercenery;
            playAudio(audio);
        }
    } else if (!!isMerceneryCallback && !isFoundAuctionTime) {
        isContinueNextProcess = true;
        let openTabConfig = { isOpenTabFirstTime: true, isOpenAuctionHouse: isOpenAuctionHouse };
        setLocalStorage('openTabConfig', openTabConfig);
    }
}

function checkTrainingAvailable() {
    let trainingLink = xpath("//a[contains(text(), 'Training')]");
    ajaxCall(trainingLink, checkTrainingCallBack);
}

function checkTrainingCallBack(resPageElement) {
    let isTrainingAvailable = xpath("//a[@class='training_button']", resPageElement);
    if (isTrainingAvailable) {
        window.location.href = xpath("//a[contains(text(), 'Training')]").href;
    }
}


function clearNotification() {
    let okButton = xpath("//div[@id='blackoutDialognotification']//input[@value='OK']");
    let yesButton = xpath("//div[@id='blackoutDialognotification']//input[@value='Yes']");
    let logInBonusButton = xpath("//div[@class='loginbonus_buttons']//input[@value='Collect Bonus']");
    if (okButton || yesButton || logInBonusButton) {
        okButton?.click();
        yesButton?.click();
        logInBonusButton?.click();
    }
}

function createAudioSound() {
    let iframeString = `<video id='audioGladiatorPlayer' autoplay controls="" hidden name="media"><source src="` + botConfig.soundGladiatorAuction + `" type="audio/mpeg"></video>`;
    let divIframeElement = document.createElement('div');
    divIframeElement.innerHTML = iframeString.trim();
    document.body.appendChild(divIframeElement);
    audioGladiator = xpath("//video[@id='audioGladiatorPlayer']");
    audioGladiator.pause();

    iframeString = `<video id='audioMerceneryPlayer' autoplay controls="" hidden name="media"><source src="` + botConfig.soundMerceneryAuction + `" type="audio/mpeg"></video>`;
    divIframeElement = document.createElement('div');
    divIframeElement.innerHTML = iframeString.trim();
    document.body.appendChild(divIframeElement);
    audioMercenery = xpath("//video[@id='audioMerceneryPlayer']");
    audioMercenery.pause();
}

function fightingWith(monster) {
    let expeditionBox = xpath("//div[@class='expedition_box']");
    let battleExpeditionListElement = document.querySelector('#expedition_list')?.parentElement;
    if (!expeditionBox) {
        battleExpeditionElement.parentElement.querySelector('a').click();
    } else if (battleExpeditionListElement) {
        let attackButton = xpath("//div[@class='expedition_box']//div[contains(., '" + monster + "')]//..//button", battleExpeditionListElement);
        attackButton.click();
    }
}

function getCurrentHp() {
    return +document.querySelector('#header_values_hp_percent').textContent.replace('%', '');
}

function getDungeonPointValue() {
    return +document.querySelector('#dungeonpoints_value_point').textContent;
}

function getExpeditionPointValue() {
    return +document.querySelector('#expeditionpoints_value_point').textContent;
}

function playAudio(audio) {
    if (audio.paused) { document.body.click(); audio.play(); }
}

function setBotConfig() {
    botConfig = JSON.parse(localStorage.getItem('botConfig'));
}

function selectPantheon() {
    let findItem = xpath("//div[@class='quest_slot_icon'][@style='background-image:url(9379/img/ui/quest/icon_items_inactive.jpg)']/../a");
    // let areana = xpath("//div[@class='quest_slot_icon'][@style='background-image:url(9379/img/ui/quest/icon_arena_inactive.jpg)']/../a");
    let defeatOppenentPantheon = xpath("//div[@class='quest_slot_icon'][@style='background-image:url(9379/img/ui/quest/icon_combat_inactive.jpg)']/../a");
    let defeatCurrentMonster = xpath("//div[@class='quest_slot_title'][contains(text(), '" + botConfig.fightMonster + "')]/../a");
    let pantheonComplete = xpath("//a[@class='quest_slot_button quest_slot_button_finish']");
    let questReroll = xpath("//div[@id='quest_footer_reroll']//input");
    if (defeatOppenentPantheon || defeatCurrentMonster || pantheonComplete || findItem) {
        pantheonComplete?.click();
        defeatOppenentPantheon?.click();
        defeatCurrentMonster?.click();
        findItem?.click();
        // areana?.click();
    } else if (questReroll && !questReroll.disabled) {
        questReroll.click();
    }
}

function trainingGladiator() {
    let trainingButton = xpath("//a[@class='training_button'][@target]");
    if (trainingButton) {
        trainingButton.click();
    }
}

function workingLabour(hour) {
    if (getCurrentHp() <= botConfig.minimumHpFighting || (getExpeditionPointValue() === 0 && getDungeonPointValue() === 0)) {
        let stableBoyItem = xpath("//th[contains(text(),'Stable boy')]");
        if (stableBoyItem) {
            xpath("//form//select[@id='workTime']").value = hour;
            xpath("//form//input[@id='hiddenJob']").value = '2';
            xpath("//form//input[@id='doWork']").click();
        } else {
            const remainingTime = !!xpath("//td[contains(text(), 'Remaining work time')]");
            const workInStable = !!xpath("//h1[contains(text(), 'Work in the stable')]");
            if (!remainingTime & !workInStable) {
                let workingLink = xpath("//a[contains(@class, 'menuitem')][text()='Work']");
                workingLink.click();
            }
        }
    }
}

// Library function
function ajaxCall(urlRequest, callbackFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            let responsePageElement = convertTextToHTMLElement('<div>' + xhr.responseText + '</div>');
            callbackFunction(responsePageElement);
        }
    };
    xhr.open('GET', urlRequest);
    xhr.send('&a=1606580967832&sh=440072d8d7494fa865a4c101fbc00831');
}

function ajaxPost(urlRequest, body, callbackFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            let responsePageElement = convertTextToHTMLElement('<div>' + xhr.responseText + '</div>');
            callbackFunction(responsePageElement);
        }
    };
    xhr.open('POST', urlRequest);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send(body);
}

function convertTextToHTMLElement(innerHTML) {
    let tempDiv = document.createElement('div'); tempDiv.innerHTML = innerHTML;
    return tempDiv.firstChild;
}

function getLocalStorage(key) {
    let value = localStorage.getItem(key)
    return JSON.parse(value ? value : null);
}

function removeLocalStorage(key) {
    localStorage.removeItem(key);
}

function setLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
}

function xpath(query, object) {
    var obj = object ?
        object.children[0]?.parentNode :
        document.children[0];
    return document.evaluate(query, obj, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

function getSecureHash() {
    let scriptText = xpath("//script[@type='text/javascript'][contains(text(),'secureHash')]").textContent;
    secureHash = scriptText.match(/var secureHash    = "(\d+|\w+)";/)[0].replace('var secureHash    = "', "").replace('";', '');
}